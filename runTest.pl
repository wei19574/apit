#!/usr/bin/perl
use strict;
use warnings;

use Getopt::Long;

my $flag = 0;
my $buildNumber;
my $project;

#get the buildnumber and project from user's input
GetOptions (
			"buildNumber=s" => \$buildNumber, 
			"project=s" => \$project
			) 
or die("Error in command line arguments!\n");

#exit with error if there is no buildNumber or project input
if (!$buildNumber){
	print "Please input the buildNumber!\n";
	$flag = 1;
	}
if (!$project){
	print "Please input the project!\n";
	$flag = 1;
	}

if ($flag == 1){
	die("Error in command line arguments!\nPlease input the complete arguments and try again!\n");
	}

system ("echo buildNumber is $buildNumber");
system ("echo project is $project");

#modify the pom file to specify the project
system ("sed -i 's/TestSuiteName/$project/g' pom.xml");

#run the test
my $result = system ("mvn test");

#modify the pom file to change it back
system ("sed -i 's/$project/TestSuiteName/g' pom.xml");

#copy the report files to the web server
system ("cd");
system ("cp -R target/surefire-reports/html d:\\webfiles\\$buildNumber");

#return the result to jenkins
if (!$result){
    system ("echo The test is successful!");
    #system ("echo result is $result");
	exit 0;
	}else{
	system ("echo The test is failed!");
	#system ("echo result is $result");
	exit 1;
	}




