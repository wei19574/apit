package com.xgAutomation.framework.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by MingWei on 2018/6/28
 * Description
 */
public class ExceptionUtil {

    public static String getErrorInfoFromException(Exception e) {
        String s = "";
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            s =  "\r\n" + sw.toString() + "\r\n";
            sw.close();
            pw.close();
        } catch (Exception e2) {
            return "ErrorInfoFromException";
        }
        return s;
    }
}
