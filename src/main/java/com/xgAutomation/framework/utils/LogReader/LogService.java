package com.xgAutomation.framework.utils.LogReader;


import com.xgAutomation.framework.meta.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志打印封装类
 * @author XG
 *
 */
public class LogService {


    private LogService(){}
    /**
     * 指定细粒度比DEBUG更低的信息事件,一般不会记录到日志文件
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void trace(Class<?> classInfo,String logType,
                             String message) {
        Logger logger = getLogger(classInfo, logType);
        logger.trace(classInfo.getSimpleName() + "#  " + message);
    }

    /**
     * 警告日志，具有潜在危害的情况打印
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void warn(Class<?> classInfo,String logType,
                            String message) {
        Logger logger = getLogger(classInfo, logType);
        logger.warn(classInfo.getSimpleName() + "#  " + message);
    }

    /**
     * debug级别  用于输出调试信息
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void debug(Class<?> classInfo, String logType,
                             String message) {
        Logger logger = getLogger(classInfo, logType);
        logger.debug(classInfo.getSimpleName() + "#  " + message);
    }

    /**
     * info日志  用于记录业务逻辑数据，方便排查问题
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void info(Class<?> classInfo, String logType,
                            String message) {
        Logger logger = getLogger(classInfo, logType);
        logger.info(classInfo.getSimpleName() + "#  " + message);
    }

    public static void info(String message) {
        info(null,null,message);
    }

    /**
     * info日志  用于记录业务逻辑数据，方便排查问题,message里面多参数
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void info(Class<?> classInfo, String logType,
                            String message, Object... objects) {
        Logger logger = getLogger(classInfo, logType);
        logger.info(classInfo.getSimpleName() + "#  " + message, objects);
    }

    /**
     * error 级别，无堆栈信息，用于业务错误日志记录，比如调用方法插入失败，业务返回失败
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void error(Class<?> classInfo, String logType,
                             String message) {
        Logger logger = getLogger(classInfo, logType);
        logger.error(classInfo.getSimpleName() + "#  " + message);
    }

    /**
     * error 级别，无堆栈信息，用于业务错误日志记录，比如调用方法插入失败，业务返回失败，有object信息
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void error(Class<?> classInfo, String logType,
                             String message, Object... objects) {
        Logger logger = getLogger(classInfo, logType);
        logger.error(classInfo.getSimpleName() + "#  " + message, objects);
    }

    /**
     * error 信息 ，有堆栈信息，用于catch异常记录，
     * @param classInfo  类信息
     * @param serialVersionUID 全局唯一id
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void error(Class<?> classInfo, String serialVersionUID, String logType,
                             String message,
                             Throwable error) {
        Logger logger = getLogger(classInfo, logType);
        logger.error( classInfo.getSimpleName() + "#  " + message, error);
    }

    /**
     * error 信息 ，有堆栈信息，用于catch异常记录，有objects信息
     * @param classInfo  类信息
     * @param logType 日志类型 参照LogType变量
     * @param message 自定义内容部分
     */
    public static void error(Class<?> classInfo, String logType,
                             String message,
                             Throwable error,Object... objects) {
        Logger logger = getLogger(classInfo, logType);
        logger.error(classInfo.getSimpleName() + "#  " + message, error, objects);
    }


    private static Logger getLogger(Class<?> classInfo, String logType) {
        Logger logger = null;
        switch (logType) {
            case LogType.BIZ:
                logger = LoggerFactory.getLogger("Biz");
                break;
            case LogType.DUBBOINVOKE:
                logger = LoggerFactory.getLogger("DubboInvoke");
                break;
            case LogType.REQUEST:
                logger = LoggerFactory.getLogger("Request");
                break;
            case LogType.Assert:
                logger = LoggerFactory.getLogger("Assert");
                break;
            case LogType.Init:
                logger = LoggerFactory.getLogger("Init");
                break;
            case LogType.DataProvider:
                logger = LoggerFactory.getLogger("DataProvider");
                break;
            case LogType.TestlinkApi:
                logger = LoggerFactory.getLogger("TestlinkApi");
                break;


        }
        return logger;
    }

}
