package com.xgAutomation.framework.utils;

import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author weim
 */
public class JiraUtil{
//	public  static HttpClientContext context = null;  
//    public  static CookieStore cookieStore = null;  
//    public  static RequestConfig requestConfig = null;
//    public  static CloseableHttpClient httpClient = null; 
//    
	static Properties prop = PropertiesFileReader.readProperties(ConfigUtils.config);
    static String uri = prop.getProperty("uri");  
    static String user = prop.getProperty("jiraUserName");  
    static String pwd = prop.getProperty("jiraPassWord");  
    static String osname = System.getProperty("os.name").toLowerCase(); 
    
    //String basic_auth = "Basic ";
//    static {  
//        init();  
//    }  
//
//    private static void init() {  
//        context = HttpClientContext.create();  
//        cookieStore = new BasicCookieStore();  
//        // 配置超时时间（连接服务端超时1秒，请求数据返回超时2秒）  
//        requestConfig = RequestConfig.custom().setConnectTimeout(120000).setSocketTimeout(60000)  
//                       .setConnectionRequestTimeout(60000).build();  
//        // 设置默认跳转以及存储cookie  
//        httpClient = HttpClientBuilder.create()  
//                     .setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy())  
//                     .setRedirectStrategy(new DefaultRedirectStrategy()).setDefaultRequestConfig(requestConfig)  
//                     .setDefaultCookieStore(cookieStore).build();  
//    }  
//    
//    public static CloseableHttpResponse post(String url,String params)  
//            throws ClientProtocolException, IOException {  
//        HttpPost httpPost = new HttpPost(url);  
//        List<NameValuePair> param = toNameValuePair(params);
//        UrlEncodedFormEntity uefentity = new UrlEncodedFormEntity(param,"UTF-8");
//        httpPost.setEntity(uefentity);  
//        CloseableHttpResponse response = httpClient.execute(httpPost, context);  
//        try {  
//            cookieStore = context.getCookieStore();  
//            List<Cookie> cookies = cookieStore.getCookies();  
//            for (Cookie cookie : cookies) {  
//                System.out.println("key:" + cookie.getName() + "  value:" + cookie.getValue());  
//            }  
//        } finally {  
//            response.close();  
//        }  
//        return response;  
//          
//    }  

    
    public static List<NameValuePair> toNameValuePair(String params){
    	List<NameValuePair> nvp = new ArrayList<NameValuePair>();
    	String[] param = params.split("&");
    	for(String nmp : param){
    		int index = 0; 
    		index = nmp.indexOf("=");
    		String key = nmp.substring(0,index);
    		String value = nmp.substring(++index, nmp.length());
    		nvp.add(new BasicNameValuePair(key, value));
    	}
    	return nvp;
  }
    //测试代码
    public static void main(String args[]){
//
//    	JiraUtil jira = new JiraUtil();
//    	Map map = new HashMap();
//    	map.put("assignee","{\"name\":\"wei19574\"}");
//    	try {
//			System.out.println(JiraUtil.createIssue("WLCSIN", "Bug", "接口测试问题", "接口测试问题",map));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
    	
              
    }   
    private static String executeShell(String command) throws IOException {  
        StringBuffer result = new StringBuffer();  
        Process process = null;  
        InputStream is = null;  
        BufferedReader br = null;  
        String line = null;  
        try {  
  
            if (osname.indexOf("windows") >= 0) {  
                process = new ProcessBuilder("cmd.exe", "/c", command).start();  
                System.out.println("cmd.exe /c " + command); //安装Cygwin，可以执行linux命令  
            } else {  
                process = new ProcessBuilder("/bin/sh", "-c", command).start();  
                System.out.println("/bin/sh -c " + command);  
            }  
  
            is = process.getInputStream();  
            br = new BufferedReader(new InputStreamReader(is, "UTF-8"));  
  
            while ((line = br.readLine()) != null) {  
                System.out.println(line);  
                result.append(line);  
            }  
  
        } catch (Exception e) {  
            // TODO Auto-generated catch block  
            e.printStackTrace();  
        } finally {  
            br.close();  
            process.destroy();  
            is.close();  
        }  
  
        return result.toString();  
    }  
  
   
  
    /** 
     * 创建工单 
     *  
     * @param projectKey 
     *            项目key 
     * @param issueType 
     *            工单类型 name 
     * @param description 
     *            工单描述 
     * @param summary 
     *            工单主
     *            工单负责人 
     * @param map 
     *            工单参数map，key为参数名称，value为参数值，参数值必须自带双引号 比如： map.put("assignee", 
     *            "{\"name\":\"username\"}"); map.put("summary", 
     *            "\"summary00002\""); 
     * @return 
     * @throws IOException 
     */  
    public static String createIssue(String projectKey, String issueType,  
            String description, String summary,Map<String,String> map) throws IOException {  
        String fields = "";  
        if (map != null && map.size() > 0) {  
            StringBuffer fieldsB = new StringBuffer();  
            
            for (Map.Entry<String, String> entry : map.entrySet()) {  
                fieldsB.append(",\"").append(entry.getKey()).append("\":")  
                        .append(entry.getValue());  
            }  
            fields = fieldsB.toString();  
        }  
  
        String command = "curl -D- -u " + user + ":" + pwd  
                + " -X POST  --data '{\"fields\": {\"project\":{ \"key\": \""  
                + projectKey + "\"},\"summary\": \"" + summary  
                + "\",\"description\": \"" + description  
                + "\",\"issuetype\": {\"name\": \"" + issueType + "\"}"  
                + fields + "}}' -H \"Content-Type: application/json\" \"" + uri  
                + "/rest/api/2/issue/\"";  
  
        String issueSt = executeShell(command);  
  
        return issueSt;  
    }  
}
