package com.xgAutomation.framework.utils.JsonReader;

import com.xgAutomation.framework.exception.FrameworkException;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by MingWei on 2018/6/22
 * Description
 */
public class JsonFileReaderUtil {


    private JsonFileReaderUtil(){}

    public static String JsonFileReader(String pathName) throws IOException, FrameworkException {
        if(pathName == null || pathName == "")
        {throw new FrameworkException("pathname不能为空");}
        File file = new File(System.getProperty("user.dir") + "/src/main/resources/dataPrepare"+pathName);
        String jsonString = FileUtils.readFileToString(file,"utf-8");
        return jsonString;
    }

    public static void main(String[] args) {
        try {
            String json = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/user.json");
            JSONObject jsonO = JSONObject.fromObject(json);
            System.out.println(jsonO.getString("areaCode"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FrameworkException f){
            f.printStackTrace();
        }
    }







}
