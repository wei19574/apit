package com.xgAutomation.framework.utils.JsonUtil;

import com.alibaba.fastjson.JSON;
import com.xgAutomation.framework.exception.FrameworkException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MingWei on 2018/6/4
 * Description
 */
public class JsonParse {

    private JsonParse() {
    }

    /**
     * @param jsonObject                          ,这个变量是拿到响应字符串通过json转换成json对象
     * @param jpath,这个jpath指的是用户想要查询json对象的值的路径写法 jpath写法举例：1) name  2)myTool[0]/test[0]/name ，myTool和test都是json数组，[0]表示索引，索引下标从0开始
     *                                            <p>
     *                                            /name 表示myTool数组下test数组某一个元素下的json对象的名称为name
     * @return，返回name这个json对象名称对应的值
     */
    public static String getValueByJpath(JSONObject jsonObject, String jpath) throws FrameworkException {

        Object obj = jsonObject;

        if (jpath == null | jsonObject == null) {
            throw new FrameworkException("请检查JsonObject或jpath的值是否为空");
        }
        //按级深入查找
        for (String s : jpath.split("/")) {
            if (!s.isEmpty()) {
                if (s.contains("[")) {
                    String value = s.split("\\[")[0];
                    int index = Integer.parseInt(s.split("\\[")[1].replaceAll("]", ""));
                    obj = ((JSONArray) ((JSONObject) obj).get(value)).get(index);
                } else {
                    obj = ((JSONObject) obj).get(s);
                }
            }
        }
        return String.valueOf(obj);
    }

    public static String getValueByJpathString(String json, String jpath) throws FrameworkException {

        JSONObject jsonObject = JSONObject.fromObject(json);

        Object obj = jsonObject;

        if (jpath == null | jsonObject == null) {
            throw new FrameworkException("请检查JsonObject或jpath的值是否为空");
        }
        //按级深入查找
        for (String s : jpath.split("/")) {
            if (!s.isEmpty()) {
                if (s.contains("[")) {
                    String value = s.split("\\[")[0];
                    int index = Integer.parseInt(s.split("\\[")[1].replaceAll("]", ""));
                    obj = ((JSONArray) ((JSONObject) obj).get(value)).get(index);
                } else {
                    obj = ((JSONObject) obj).get(s);
                }
            }
        }
        return String.valueOf(obj);
    }




    /**
     * 找到路径下的key，替换key里面的值 只支持一级数组
     *
     * @param jsonObject
     * @param jpath
     * @return
     * @throws FrameworkException
     */

    public static String replaceValueByJpath(JSONObject jsonObject, String jpath, String replace) throws FrameworkException {

        Object obj = jsonObject;
        Object lastObject = jsonObject;
        JSONObject objectLast = jsonObject;
        if (jpath == null | jsonObject == null) {
            throw new FrameworkException("请检查JsonObject或jpath的值是否为空");
        }
        String value = null;
        int index = 0;

        //按级深入查找
        for (String s : jpath.split("/")) {
            if (!s.isEmpty()) {

                if (s.contains("[")) {
                    value = s.split("\\[")[0];
                    index = Integer.parseInt(s.split("\\[")[1].replaceAll("]", ""));
                    obj = ((JSONArray) ((JSONObject) obj).get(value)).get(index);
                    JSONArray jsonArray = objectLast.getJSONArray(value);
                    jsonArray.set(index, (JSONObject) obj);
                    objectLast.put(value, jsonArray);
                } else {
                    ((JSONObject) obj).put(s, replace);
                }


            }
        }


        return objectLast.toString();
    }

    public static String replaceValueByJpathString(String json, String jpath, String replace) throws FrameworkException {
        JSONObject jsonObject = JSONObject.fromObject(json);
        if (jsonObject == null) {
            throw new IllegalArgumentException("json输入不合法");
        }
        Object obj = jsonObject;
        Object lastObject = jsonObject;
        JSONObject objectLast = jsonObject;
        if (jpath == null | jsonObject == null) {
            throw new FrameworkException("请检查JsonObject或jpath的值是否为空");
        }
        String value = null;
        int index = 0;

        //按级深入查找
        for (String s : jpath.split("/")) {
            if (!s.isEmpty()) {

                if (s.contains("[")) {
                    value = s.split("\\[")[0];
                    index = Integer.parseInt(s.split("\\[")[1].replaceAll("]", ""));
                    obj = ((JSONArray) ((JSONObject) obj).get(value)).get(index);
                    JSONArray jsonArray = objectLast.getJSONArray(value);
                    jsonArray.set(index, (JSONObject) obj);
                    objectLast.put(value, jsonArray);
                } else {
                    ((JSONObject) obj).put(s, replace);
                }


            }
        }


        return objectLast.toString();
    }

    public static void main(String[] args) {
        String json = "{\"age\":21," +
                "\"myTool\":[{\"name\":\"自行车\",\"price\":267,\"test\":[{\"name\":100}]},{\"name\":\"摩托车\",\"price\":3267}," +
                "{\"name\":\"小汽车\"," +
                "\"price\":100000}],\"name\":\"张三\",\"sex\":\"男\"}";
        String s = "{\"age\":21,\"myTool\":[{\"name\":\"自行车\",\"price\":267,\"test\":[{\"name\":\"100\"},{\"price\":150}]},{\"name\":\"摩托车\",\"price\":3267},{\"name\":\"小汽车\",\"price\":100000}],\"name\":\"张三\"}";
        JSONObject jsonObjects = JSONObject.fromObject(json);

//            System.out.println(JsonParse.getValueByJpath(jsonObject,"myTool[0]/test[0]/name"));
        try {
            System.out.println(JsonParse.replaceValueByJpath(jsonObjects, "myTool[0]/test[0]/name", "88"));
        } catch (FrameworkException e) {
            e.printStackTrace();
        }

    }
}
