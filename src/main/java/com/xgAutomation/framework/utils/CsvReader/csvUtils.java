package com.xgAutomation.framework.utils.CsvReader;

import org.apache.commons.lang.ArrayUtils;
import org.testng.annotations.DataProvider;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class csvUtils {
	
	
	
	public static Object[][] csvReader(String filePath){
		
		String line;
		List<String[]> list = new ArrayList<String[]>();
		
		BufferedReader br;
		try {
//			br = new BufferedReader(new FileReader(filePath));
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
			//跳过第一行
			br.readLine();
			//继续读取数据
			while((line = br.readLine()) != null){
				String[] datas = line.split(",");
				//去掉每行第一个数据
				datas = (String[])ArrayUtils.removeElement(datas,datas[0]);
				for (int i = 0; i < datas.length; i++){
					datas[i] = datas[i].replaceAll("##", ",");
				}				
				list.add(datas);				 	
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Object[][] ob = new Object[list.size()][];
		for(int i=0;i<list.size();i++){
			//System.out.println(Arrays.toString(list.get(i)));
			ob[i] = list.get(i);
		}
		return ob;
	}
	
//	public  static void main(String[] args){
//		Object [][] a = csvUtils.csvReader("InterfaceTestcase/interface.csv");
//		for(int i=0;i<a.length;i++){
//		System.out.println(Arrays.toString(a[i]));
//				};
//	}
	
	@DataProvider
	public Object[][] dataPro() {
		Object [][] a = csvUtils.csvReader("InterfaceTestcase/interface.csv");
		return a;
	}

	
		
	}

