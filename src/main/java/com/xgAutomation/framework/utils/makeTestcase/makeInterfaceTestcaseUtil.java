package com.xgAutomation.framework.utils.makeInterfaceTestcase;

import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.CsvReader.csvUtils;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import com.xgAutomation.framework.utils.ConfigUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class makeInterfaceTestcaseUtil {
		private Configuration config;
		public makeInterfaceTestcaseUtil mtu;
		private static String csvpath;
		private static String SourcetemplateFile;
		//初始化数据
		@BeforeClass
		public void init() throws IOException {
			config = new Configuration();
			config.setDirectoryForTemplateLoading(new File("template"));
			Properties prop = PropertiesFileReader.readProperties(ConfigUtils.makeInterfaceTestcase);
			//csv所在路径
			csvpath = prop.getProperty("tokoonderdilInterfaceTestcasecsvpath");
			//代码模板路径
			SourcetemplateFile = prop.getProperty("tokoonderdilSourcetemplateFile");
		}
		@Test
		(dataProvider = "dataPro",testName = "make")
		public void make(String testcaseDir,String csvDir,String interface_name,String interface_description,
						 String interfaceUrl,String param,String isRun) throws IOException{
			if(isRun.equals("Y")){
				LogService.info(makeInterfaceTestcaseUtil.class, LogType.BIZ,"开始创建"+interface_name+"("+interface_description+")"+"接口");
			Map<String,String> mp = new HashMap<String, String>();


			String input_param = "";
			String modifyParam = "";
			String sourceParam = "";
			//处理传入多个参数
			if(param == null||param.length()==0){
				input_param="";
				modifyParam="";
			}else if(param != "" || param.length()>0){
				String [] paramArray = param.split("&");
				if(paramArray.length > 1){
					for(String a : paramArray){
					input_param += "String "+a+",";
					modifyParam += a+",";
					sourceParam += "            "+"bizparams.put("+"\""+a+"\""+", "+a+");"+"\n";
 					}
				}else{
					input_param = "String "+param+",";
					modifyParam = param+",";
					sourceParam += "            "+"bizparams.put("+"\""+param+"\""+", "+param+");";
					 }
			}
			String packageDir = ("com/xgAutomation/testcase" + testcaseDir).replace("/",".");
			mp.put("interface_name", interface_name);
			mp.put("input_param", input_param);
			mp.put("modifyParam", modifyParam);
			mp.put("testcaseDir", testcaseDir);
			mp.put("interfaceUrl", interfaceUrl);
			mp.put("sourceParam", sourceParam);
			mp.put("packageDir", packageDir);
			mp.put("csvDir", csvDir);
			String sourceFileName = interface_name + ".java";
            String sourceSavaPath = testcaseDir + "/";
			Template sourceTemplate = config.getTemplate(SourcetemplateFile);
			makeInterfaceTestcaseUtil.bulid(mp, sourceSavaPath, sourceFileName, sourceTemplate);
		}else if(isRun.equals("N")){
				LogService.info(makeInterfaceTestcaseUtil.class, LogType.BIZ,"isRun为N，无须创建"+interface_name+"("+interface_description+")"+"接口");
	}else{
		System.out.println("请检查isRun的值是否正确");
	}
		}
		public static void bulid(Map<String,String> mp,String savePath,String fileName,Template template) throws IOException {
			
			String file_name = savePath + fileName;
			System.out.println(file_name);
			File directory = new File(savePath);
			if(!directory.exists()){
				directory.mkdirs();
			}
			File file = new File(savePath);
			if (file.isFile()) {
				throw new AssertionError(fileName + "文件已经存在，请删除已存在文件后重新运行脚本");
			}else{
				file.createNewFile();
			}
			try {
				Writer wr = new OutputStreamWriter(new FileOutputStream(file_name));
				template.process(mp,wr);
				wr.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TemplateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@DataProvider
		public Object[][] dataPro(){
			return csvUtils.csvReader(csvpath);
		}



}

