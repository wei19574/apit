package com.xgAutomation.framework.utils.PropertyReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * @author
 * @Date 2016-07-11
 */
public class PropertiesFileReader {
    private static PropertiesFileReader INSTANCE = null;
    static Properties properties = null;
    static InputStream in = null;

    private PropertiesFileReader() {

    }

    public static synchronized PropertiesFileReader getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PropertiesFileReader();
        }
        return INSTANCE;
    }

    public static Properties readProperties(String name) {
    	properties = new Properties();
        if (null != name) {
            in = properties.getClass().getResourceAsStream("/" + name);
            try {
                properties.load(new InputStreamReader(in, "utf-8"));
            }catch (IOException e){
                e.printStackTrace();
            }

        }
        return properties;
    }

    public static String getProperty(String key) {
        String value = null;
        if (null != key && !"".equalsIgnoreCase(key)) {
            value = (String) properties.get(key);
        }
        return value;
    }

    public static int getPropertyInt(String key) {
        String value = null;
        if (null != key && !"".equalsIgnoreCase(key)) {
            value = (String) properties.get(key);
        }
        return Integer.valueOf(value);
    }

    public static String getQuery(String query) {
        String value = null;
        if (null != query && !"".equalsIgnoreCase(query)) {
            value = (String) properties.get(query);
        }
        return value;
    }

}

