package com.xgAutomation.framework.utils;


import org.apache.commons.lang.RandomStringUtils;


import java.io.UnsupportedEncodingException;
import java.util.Random;

public class RandomUtil {
	
	 //生成指定位数的汉字
    public static String getRandomJianHan(int len)
    {
        String ret="";
          for(int i=0;i<len;i++){
              String str = null;
              int hightPos, lowPos; // 定义高低位
              Random random = new Random();
              hightPos = (176 + Math.abs(random.nextInt(39))); //获取高位值
              lowPos = (161 + Math.abs(random.nextInt(93))); //获取低位值
              byte[] b = new byte[2];
              b[0] = (new Integer(hightPos).byteValue());
              b[1] = (new Integer(lowPos).byteValue());
              try
              {
                  str = new String(b, "GBk"); //转成中文
              }
              catch (UnsupportedEncodingException ex)
              {
                  ex.printStackTrace();
              }
               ret+=str;
          }
      return ret;
    }
    
  //生成指定位数的随机字符串
    public static String getRandomNumStringhex(int num){
		String a = RandomStringUtils.random(num, new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q'
							,'r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M',
							'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'});
		return a;
	}
    
    
    public static int getRandomNum(int min,int max){
		Random random = new Random();
		int i = random.nextInt(max-min+1)+min;
		return i;
	}
    
    
}
