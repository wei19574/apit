package com.xgAutomation.framework.utils.Interceptor;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by MingWei on 2018/3/31
 * Description
 */
public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        Request updataRequest = request.newBuilder()
                .header("mac","11-22-33-44-55-66-77-88")
                .build();
        Response res = chain.proceed(updataRequest);
        return res;
    }
}
