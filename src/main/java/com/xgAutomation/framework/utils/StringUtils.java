package com.xgAutomation.framework.utils;

import java.util.Random;

/**
 * Created by MingWei on 2018/6/26
 * Description
 */
public class StringUtils {
    /**
     * 生成长度为length的随机字符串
     * @param length
     * @return
     */
    public static String getRandomString(int length){
        //定义一个字符串（A-Z，a-z，0-9）即62位；
        String str="zxcvbnmlkjhgfdsaqwertyuiopQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
        //由Random生成随机数
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        //长度为几就循环几次
        for(int i=0; i<length; ++i){
            //产生0-61的数字
            int number=random.nextInt(62);
            //将产生的数字通过length次承载到sb中
            sb.append(str.charAt(number));
        }
        //将承载的字符转换成字符串
        return sb.toString();
    }

    /**
     * 生成长度为length的随机数字
     * @param length
     * @return
     */
    public static Integer getRandomNumber(int length){
        //定义一个字符串（A-Z，a-z，0-9）即62位；
        String str="1234567890";
        //由Random生成随机数
        Random random=new Random();
        StringBuffer sb=new StringBuffer();
        //长度为几就循环几次
        for(int i=0; i<length; ++i){
            //产生0-61的数字
            int number=random.nextInt(9);
            //将产生的数字通过length次承载到sb中
            sb.append(str.charAt(number));
        }
        //将承载的字符转换成字符串
        return Integer.valueOf(sb.toString());
    }


    /**
     * 获取随机邮箱
     * @return
     */
    public static String getRandomEmail(){
        StringBuffer stringBuffer = new StringBuffer();
        String emailBack = "@"+ getRandomNumber(3) + ".com";
        String emailFront = "test_";
        String emailRandom = getRandomString(10);
        stringBuffer.append(emailFront).append(emailRandom).append(emailBack);
        return  stringBuffer.toString();
    }



    //JIRA格式处理
    public static String JiraResHandle(String str){
        int index = str.indexOf("}");
        String res = str.substring(1,index).replaceAll("\"","");
        String res1 = res.replaceAll("\\{","").replaceAll("/", "");
        return res1;
    }

    //格式转换 String to Integer
    public static Integer stringToInt(String str){
        Integer a = 0;
        if(str == null||str == ""){
            a = null;
        }else if(str!= null){
            a = Integer.parseInt(str);
        }/*else{
    		System.out.println("请检查格式");
    	}*/
        return a;
    }

    //格式转换 String to Long
    public static Long stringToLong(String str){
        Long a = null ;
        if(str == null||str == ""){
            a = null;
        }else if(str!= null){
            a = Long.valueOf(str);
        }/*else{
    		System.out.println("请检查格式");
    	}*/
        return a;
    }

    //格式转换 String to Double
    public static Double stringToDouble(String str){
        Double a = null ;
        if(str == null||str == ""){
            a = null;
        }else if(str!= null){
            a = Double.valueOf(str);

        }/*else{
    		System.out.println("请检查格式");
    	}*/
        return a;
    }

    public static String remove(String resource,char ch){

        int position = 0;
        StringBuffer buffer = new StringBuffer();
        char CurrentChar;

        while(position < resource.length()){
            CurrentChar = resource.charAt(position++);
            if(CurrentChar != ch){
                buffer.append(CurrentChar);
            }
        }
        return buffer.toString();
    }

    public static String removeBackspace(String resource){

        return resource.replaceAll("\\s*", "");

    }


    public static void main(String[] args) {
        System.out.println(getRandomEmail());
    }
}
