package com.xgAutomation.framework.utils.Assert;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.StringUtils;
import net.sf.json.JSONObject;
import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.AbstractCharSequenceAssert;
import org.assertj.core.api.AbstractObjectArrayAssert;
import org.assertj.core.api.AbstractObjectAssert;


import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AssertUtil {

    private AssertUtil() {

    }


    public static AbstractCharSequenceAssert<?, String> checkCode(Integer actualCode, String expectedCode) throws FrameworkException {

        if (actualCode != null && actualCode >= 0) {
            if (expectedCode != null && !expectedCode.equals("")) {
                String code = actualCode.toString();
                LogService.info(AssertUtil.class, LogType.Assert, "预期code为: {} 实际code为: {}", expectedCode, actualCode);
                return assertThat(code).isEqualTo(expectedCode);
            } else {
                LogService.info(AssertUtil.class, LogType.Assert, "实际code为: {} ,expectedCode没有赋值，不做校验", actualCode);
                return null;
            }
        } else {
            throw new FrameworkException("actualCode值没有正确返回");
        }
    }

    public static AbstractCharSequenceAssert<?, String> checkMessage(JSONObject actualMessage, String expectedMessage, String message) throws FrameworkException {

        if (null != actualMessage && null != message) {
            if (null != expectedMessage && !expectedMessage.equals("")) {
                String mes = actualMessage.getString(message);
                LogService.info(AssertUtil.class, LogType.Assert, "预期message为: {} 实际message为: {}", expectedMessage, mes);
                return assertThat(mes).isEqualTo(expectedMessage);
            } else {
                LogService.info(AssertUtil.class, LogType.Assert, "实际message为: {} ,expectedMessage没有赋值，不做校验", actualMessage);
                return null;
            }
        } else {
            throw new FrameworkException("请检查actualMessage或message的值是否为null");
        }
    }

    public static AbstractBooleanAssert<?> checkIsSuccess(Boolean actualIsSucesss, String expectedIsSucesss) throws FrameworkException {

        if (actualIsSucesss != null) {
            if (expectedIsSucesss != null && !expectedIsSucesss.equals("")) {
                Boolean expectedIsSucesssB = Boolean.parseBoolean(expectedIsSucesss);
                LogService.info(AssertUtil.class, LogType.Assert, "预期isSuccess为: {} 实际isSuccess为: {}", expectedIsSucesss, actualIsSucesss);
                return assertThat(actualIsSucesss).isEqualTo(expectedIsSucesssB);
            } else {
                LogService.info(AssertUtil.class, LogType.Assert, "实际isSuccess为: {} ,expectedIsSucesss没有赋值，不做校验", actualIsSucesss);
                return null;
            }
        } else {
            throw new FrameworkException("actualIsSucesss值没有正确返回");
        }
    }


    public static AbstractCharSequenceAssert<?, String> checkData(String actualData, String expectedData) throws FrameworkException {

        if (null == actualData) {
            throw new NullPointerException("actualData的值为空");
        }

        if (null != expectedData && !expectedData.equals("")) {
                checkJsonObjectData(actualData,expectedData);
            return null;
        } else {
            LogService.info(AssertUtil.class, LogType.Assert, "实际data为: {}, expectedData没有赋值，不做校验", actualData);
            return null;
        }

    }

    private static void checkJsonObjectData(String actualData, String expectedData){
        if (expectedData.split("&").length > 1) {
            String[] s = expectedData.split("&");
            for (int i = 0; i < s.length; i++) {
                assertThat(actualData.replaceAll("\"", "")).contains(s[i]);
                LogService.info(AssertUtil.class, LogType.Assert, "预期包含的data为: {} 实际data为: {}", s[i], s[i].split(":")[0] + ":" + actualData);
            }
        }else{
            assertThat(actualData.replaceAll("\"", "")).contains(expectedData);
            LogService.info(AssertUtil.class, LogType.Assert, "预期包含的data为: {} 实际data为: {}", expectedData, actualData);
        }

    }

    public static AbstractCharSequenceAssert<?, String> checkTestlinkResult(String actualResult,String expectedResult){
        LogService.info(AssertUtil.class, LogType.Assert, "预期包含的result为: {} 实际result为: {}", expectedResult, actualResult);
        return assertThat(actualResult).contains(StringUtils.removeBackspace(expectedResult));
    }

    /**
     * 预期结果多参数校验
     *
     * @param actual
     * @param expected
     * @return
     */
    public static AbstractObjectAssert TestResult(Map<String,Object> actual, String expected){
        return assertThat(actual).containsKeys(new String[]{"name","id"});
    }

    public static void main(String[] args) {
        Map<java.lang.String,Object> mp = new HashMap<>();
        mp.put("name","weim");
        mp.put("id",new Integer(5));
        TestResult(mp,"name");

    }
}
