package com.xgAutomation.framework.utils.TestNG.TestNGUtils;

import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.utils.Encrypt.EncodeUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AssertionResponse {
	
	Response res;
	
	public AssertionResponse (Response res){
		this.res = res;
	}
	
	public boolean checkCode(String expectedCode){
		String expression = "\"code\":\"(.*?)\"";
		String expression1 = "\"code\":(\\d*)";
		String expression2 = "\"resultCode\":(\\d*)";
		Pattern p = Pattern.compile(expression);
		Matcher m = p.matcher(res.getBody());	
		Pattern p1 = Pattern.compile(expression1);
		Matcher m1 = p1.matcher(res.getBody());
		Pattern p2 = Pattern.compile(expression2);
		Matcher m2 = p2.matcher(res.getBody());
		boolean result = false;
		if (m.find()) {					
			System.out.println("code: " + m.group(1));
			System.out.println("expectedCode: " + expectedCode);
			if (m.group(1).equals(expectedCode)){
				System.out.println("The code check passed!");
				result = true;
			}else{
				System.out.println("The code check failed!");
			}
		}else if(m1.find()){
			System.out.println("code: " + m1.group(1));
			System.out.println("expectedCode: " + expectedCode);
			if (m1.group(1).equals(expectedCode)){
				System.out.println("The code check passed!");
				result = true;
			}else{
				System.out.println("The code check failed!");
			}
		}else if(m2.find()){
			System.out.println("code: " + m2.group(1));
			System.out.println("expectedCode: " + expectedCode);
			if (m2.group(1).equals(expectedCode)){
				System.out.println("The code check passed!");
				result = true;
			}else{
				System.out.println("The code check failed!");
			}
		}
		else {
			System.out.println("code没有返回符合格式的内容");
		}
		return result;
	}
	
    public boolean checkMessage(String expectedMessage,String InterfaceTestcaseType){
    	boolean result = false;
    	if(InterfaceTestcaseType.equals("反例")){
	    	String expression = "\"message\":\"(.*?)\"";
	    	String expression1 = "\"msg\":\"(.*?)\"";
	    	String expression2 = "\"errorMsg\":\"(.*?)\"";
	    	if (expectedMessage.equals("null")){
	    		expression = "\"message\":(null)";
	    	}
	    	Matcher m = Pattern.compile(expression).matcher(res.getBody());
			Matcher m1 = Pattern.compile(expression1).matcher(res.getBody());
			Matcher m2 = Pattern.compile(expression2).matcher(res.getBody());
			if (m.find()) {					
				System.out.println("message: " + m.group(1));
				System.out.println("expectedMessage: " + expectedMessage);
				if (m.group(1).equals(expectedMessage)){
					System.out.println("The message check passed!");
					result = true;
				}else{
					System.out.println("The message check failed!");		
				}
			} else if(m1.find()){					
					System.out.println("message: " + m1.group(1));
					System.out.println("expectedMessage: " + expectedMessage);
					if (m1.group(1).equals(expectedMessage)){
						System.out.println("The message check passed!");
						result = true;
					}else{
						System.out.println("The message check failed!");
						 }				
			}else if(m2.find()){
					System.out.println("message: " + m2.group(1));
					System.out.println("expectedMessage: " + expectedMessage);
					if (m2.group(1).equals(expectedMessage)){
						System.out.println("The message check passed!");
						result = true;
					}else{
						System.out.println("The message check failed!");
					}		
			}else{
					System.out.println("message没有返回符合格式的内容");
			}
    	}else{
    		result = true;
    	}
		return result;		
	}
    //无论正例反例均做校验
    public boolean checkUnicodeMessage(String expectedMessage){
    	boolean result = false;
    	
	    	String expression = "\"message\":\"(.*?)\"";
	    	String expression1 = "\"msg\":\"(.*?)\"";
	    	String expression2 = "\"errorMsg\":\"(.*?)\"";
	    	if (expectedMessage.equals("null")){
	    		expression = "\"message\":(null)";
	    	}
	    	Matcher m = Pattern.compile(expression).matcher(res.getBody());
			Matcher m1 = Pattern.compile(expression1).matcher(res.getBody());
			Matcher m2 = Pattern.compile(expression2).matcher(res.getBody());
			if (m.find()) {					
				System.out.println("message: " + EncodeUtil.convertUnicode(m.group(1)));
				System.out.println("expectedMessage: " + expectedMessage);
				if (EncodeUtil.convertUnicode(m.group(1)).equals(expectedMessage)){
					System.out.println("The message check passed!");
					result = true;
				}else{
					System.out.println("The message check failed!");		
				}
			} else if(m1.find()){					
					System.out.println("message: " + EncodeUtil.convertUnicode(m1.group(1)));
					System.out.println("expectedMessage: " + expectedMessage);
					if (EncodeUtil.convertUnicode(m1.group(1)).equals(expectedMessage)){
						System.out.println("The message check passed!");
						result = true;
					}else{
						System.out.println("The message check failed!");
						 }				
			}else if(m2.find()){
					System.out.println("message: " + m2.group(1));
					System.out.println("expectedMessage: " + expectedMessage);
					if (m2.group(1).equals(expectedMessage)){
						System.out.println("The message check passed!");
						result = true;
					}else{
						System.out.println("The message check failed!");
					}		
			}else{
					System.out.println("message没有返回符合格式的内容");
			}
    	
		return result;		
	}
    
    public boolean checkResult(String resultRegex,String InterfaceTestcaseType){
        
    	boolean result = false;
    	if(InterfaceTestcaseType.equals("正例")){
    		if(resultRegex.equals("") ||resultRegex.length() == 0){
    			result = true;
    		}else{
    			if(result = matchResult(resultRegex).equals(true)){
    			return result;
    			}else{
    				result = matchData(resultRegex);
    			     }
    			 }
    	}else{
    		result = true;
    	}
    	return result;
    }
    
    public Boolean matchResult(String resultRegex){
    	Boolean bool = false;
    	String expression = "\"result\":(.*)\\S";   	
		Pattern p = Pattern.compile(expression);
		Matcher m = p.matcher(res.getBody());
			if (m.find()) {
				System.out.println("result: " + m.group(1));
				System.out.println("resultRegex: " + resultRegex);
				if (resultRegex.equals("null")){
					if(m.group(1).equals("null")){
						bool = true;
						System.out.println("The result check passed!");
					}else{
						System.out.println("The result check failed!");
					}
					return bool;
				}
				String resultExpression = resultRegex;			
				Pattern resultPattern = Pattern.compile(resultExpression);
				Matcher resultMatch = resultPattern.matcher(m.group(1));
				if (resultMatch.find( )){
					System.out.println("The result check passed!");
					bool = true;
				}else{
					System.out.println("The result check failed!");
				}
    	}
			return bool;
    }
    public Boolean matchData(String resultRegex){
    	Boolean bool = false;
    	String expression = "\"data\":(.*)\\S";   	
		Pattern p = Pattern.compile(expression);
		Matcher m = p.matcher(res.getBody());
			if (m.find()) {
				System.out.println("data: " + m.group(1));
				System.out.println("dataRegex: " + resultRegex);
				if (resultRegex.equals("null")){
					if(m.group(1).equals("null")){
						bool = true;
						System.out.println("The data check passed!");
					}else{
						System.out.println("The data check failed!");
					}
					return bool;
				}
				String resultExpression = resultRegex;			
				Pattern resultPattern = Pattern.compile(resultExpression);
				Matcher resultMatch = resultPattern.matcher(m.group(1));
				if (resultMatch.find( )){
					System.out.println("The data check passed!");
					bool = true;
				}else{
					System.out.println("The data check failed!");
				}
    	}
			return bool;
    }
   
    
}
