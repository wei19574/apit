package com.xgAutomation.framework.utils.TestNG.TestNGReportUtil;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.ResourceCDN;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.TestAttribute;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import org.testng.*;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by MingWei on 2018/7/2
 * Description
 */

public class IreporterListener implements IReporter {
    //生成的路径以及文件名
    private static final String OUTPUT_FOLDER = "reportOutput/";
    private static final String FILE_NAME = "RPindex.html";
    private ExtentReports extent;


    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        initialization();
//        boolean createSuitNode = false;

//        if (suites.size() > 1) {
//            createSuitNode = true;
//        }

        for (ISuite suite : suites) {
            Map<String, ISuiteResult> resultMap = suite.getResults();

            if (resultMap.size() == 0) {
                continue;
            }

            //统计suite下的成功、失败、跳过的总用例数
            int suiteFailSize = 0;
            int suitePassSize = 0;
            int suiteSkipSize = 0;
            ExtentTest suiteTest = null;

//            if (createSuitNode) {
//                suiteTest = extent.createTest(suite.getName()).assignCategory(suite.getName());
//            }

            boolean createSuiteResultNode = false;

            if (resultMap.size() > 1) {
                createSuiteResultNode = true;
            }

            for (ISuiteResult isr : resultMap.values()) {
                ExtentTest resultNode;
                ITestContext context = isr.getTestContext();
                if (createSuiteResultNode) {
                    if (suiteTest == null) {
                        resultNode = extent.createTest(isr.getTestContext().getName());
                    } else {
                        resultNode = suiteTest.createNode(isr.getTestContext().getName());
                    }
                } else {
                    resultNode = suiteTest;
                }

                if (resultNode != null) {
                    resultNode.getModel().setName(suite.getName() + " : " + isr.getTestContext().getName());
                    if (resultNode.getModel().hasCategory()) {
                        resultNode.assignCategory(isr.getTestContext().getName());
                    } else {
                        resultNode.assignCategory(suite.getName(), isr.getTestContext().getName());
                    }
                    resultNode.getModel().setStartTime(isr.getTestContext().getStartDate());
                    resultNode.getModel().setEndTime(isr.getTestContext().getEndDate());
                    //统计SuiteResult下的数据
                    int passSize = isr.getTestContext().getPassedTests().size();
                    int failSize = isr.getTestContext().getFailedTests().size();
                    int skipSize = isr.getTestContext().getSkippedTests().size();
                    suitePassSize += passSize;
                    suiteFailSize += failSize;
                    suiteSkipSize += skipSize;
                    if (failSize > 0) {
                        resultNode.getModel().setStatus(Status.FAIL);
                    }
                    resultNode.getModel().setDescription(String.format("Pass: %s ; Fail: %s ; Skip: %s ;", passSize, failSize, skipSize));
                }
                buildTestNodes(resultNode, context.getFailedTests(), Status.FAIL);
                buildTestNodes(resultNode, context.getSkippedTests(), Status.SKIP);
                buildTestNodes(resultNode, context.getPassedTests(), Status.PASS);
            }
            if (suiteTest != null) {
                suiteTest.getModel().setDescription(String.format("Pass: %s ; Fail: %s ; Skip: %s ;", suitePassSize, suiteFailSize, suiteSkipSize));
                if (suiteFailSize > 0) {
                    suiteTest.getModel().setStatus(Status.FAIL);
                }
            }

        }
        for (String s : Reporter.getOutput()) {
            extent.setTestRunnerOutput(s);
        }

        extent.flush();
    }


    private void buildTestNodes(ExtentTest extenttest, IResultMap tests, Status status) {
        //存在父节点时，获取父节点的标签
        String[] categories = new String[0];
        if (extenttest != null) {
            List<TestAttribute> categoryList = extenttest.getModel().getCategoryContext().getAll();
            categories = new String[categoryList.size()];
            for (int index = 0; index < categoryList.size(); index++) {
                categories[index] = categoryList.get(index).getName();
            }
        }

        ExtentTest test;

        if (tests.size() > 0) {
            //调整用例排序，按时间排序
            Set<ITestResult> treeSet = new TreeSet<ITestResult>(new Comparator<ITestResult>() {
                @Override
                public int compare(ITestResult o1, ITestResult o2) {
                    return o1.getStartMillis() < o2.getStartMillis() ? -1 : 1;
                }
            });
            treeSet.addAll(tests.getAllResults());
            for (ITestResult result : treeSet) {
                Object[] parameters = result.getParameters();
                String name = "";
                //如果有参数，则使用参数的toString组合代替报告中的name
//                for(Object param:parameters){
//                    name+=param.toString();
//                }
                String summary = String.valueOf(parameters[0]);
                String url = String.valueOf(parameters[1]);
                String interfaceName = url.substring(url.indexOf("/", 1), url.length());
                name = name + summary + interfaceName;

                if (extenttest == null) {
                    test = extent.createTest(name);
                } else {
                    //作为子节点进行创建时，设置同父节点的标签一致，便于报告检索。
                    test = extenttest.createNode(name).assignCategory(categories);
                }
                //test.getModel().setDescription(description.toString());
                //test = extent.createTest(result.getMethod().getMethodName());
                for (String group : result.getMethod().getGroups())
                    test.assignCategory(group);

                List<String> outputList = Reporter.getOutput(result);
                for (String output : outputList) {
                    //将用例的log输出报告中
                    test.debug(output);
                }
                if (result.getThrowable() != null) {
                    test.log(status, result.getThrowable());
                } else {
                    test.log(status, "Test " + status.toString().toLowerCase() + "ed");
                }

                test.getModel().setStartTime(getTime(result.getStartMillis()));
                test.getModel().setEndTime(getTime(result.getEndMillis()));
            }
        }
    }

    private void initialization() {

        File file = new File(OUTPUT_FOLDER);
        if (!file.exists() && !file.isDirectory()) {
            file.mkdir();
        }
        String filepath = OUTPUT_FOLDER +FILE_NAME;

        ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(filepath);
        //设置标题
        extentHtmlReporter.config().setDocumentTitle("api自动化测试");
        //设置报告名称
        extentHtmlReporter.config().setReportName("api自动化测试报告");
        extentHtmlReporter.config().setChartVisibilityOnOpen(true);
        extentHtmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);

        extentHtmlReporter.config().setResourceCDN(ResourceCDN.EXTENTREPORTS);
        extentHtmlReporter.config().setCSS(".node.level-1  ul{ display:none;} .node.level-1.active ul{display:block;}");
        extent = new ExtentReports();
        extent.attachReporter(extentHtmlReporter);
        extent.setReportUsesManualConfiguration(true);
    }

    public Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }

    public static void main(String[] args) {
        SimpleDateFormat df = new SimpleDateFormat("MM-dd-HH-mm-ss");//设置日期格式
        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
    }


}

