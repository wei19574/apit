package com.xgAutomation.framework.utils.mail;

import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * @author weim
 * Created Time: 2018/6/28 上午10:21
 */
public class MailUtil {
    public static Properties mailProp = PropertiesFileReader.readProperties(ConfigUtils.mailConfig);

    /**
     * 邮件发送器
     *
     * @return 配置好的工具
     */
    private static JavaMailSenderImpl createMailSender() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(mailProp.getProperty("mailHost"));
        sender.setPort(Integer.valueOf(mailProp.getProperty("mailPort")));
        sender.setUsername(mailProp.getProperty("mailUsername"));
        sender.setPassword(mailProp.getProperty("mailPassword"));
        sender.setDefaultEncoding("Utf-8");
        Properties p = new Properties();
        p.setProperty("mail.smtp.timeout", mailProp.getProperty("mailTimeout"));
        p.setProperty("mail.smtp.auth", "false");
        p.put("mail.smtp.starttls.enable", "true");
        sender.setJavaMailProperties(p);
        return sender;
    }

    private static JavaMailSenderImpl mailSender = createMailSender();

    /**
     * 发送邮件
     *
     * @param to 接受人
     * @param subject 主题
     * @param mailHtml 发送html内容
     * @throws MessagingException 异常
     * @throws UnsupportedEncodingException 异常
     */
    public void sendMail(InternetAddress[] to, String subject, String mailHtml, String fileName, String filePath) throws MessagingException,UnsupportedEncodingException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        // 设置utf-8或GBK编码，否则邮件会有乱码
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

        messageHelper.setFrom(mailProp.getProperty("mailFrom"), mailProp.getProperty("personal"));
        messageHelper.setTo(to);
        messageHelper.setSubject(subject);
        messageHelper.setText(mailHtml, true);
        messageHelper.addAttachment(fileName,new File(filePath));

        mailSender.send(mimeMessage);
    }
}
