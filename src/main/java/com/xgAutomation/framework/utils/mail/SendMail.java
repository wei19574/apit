package com.xgAutomation.framework.utils.mail;

import javax.mail.internet.AddressException;
import java.io.IOException;

/**
 * Created by MingWei on 2018/7/6
 * Description
 */
public class SendMail {
    public static void main(String[] args) {
        MailListener mailListener = null;
        try {
            mailListener = new MailListener();
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mailListener.failMailTest();
    }
}
