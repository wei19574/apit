package com.xgAutomation.framework.utils.mail;

import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * @author weim
 * Created Time: 2018/6/28 上午14:21
 */
public class MailListener {
    /**
     * 邮件主题
     */
    public String subject;
    /**邮件组*/
    public InternetAddress[] testAddressList;
    /**邮件内容*/
    public String mailHtml;

    public MailListener() throws AddressException, IOException{

        //testAddressList = new InternetAddress[]{new InternetAddress("wm195723931@163.com"),new InternetAddress("xiaoyan.zhang@xinguang.com")};
        testAddressList = new InternetAddress[]{new InternetAddress("weim@xinguangnet.com"),new InternetAddress("qunqun.liu@xinguang.com"),new InternetAddress("lishujia@xinguang.com"),new InternetAddress("xiaoyan.zhang@xinguang.com")};
    }

    public void failMailTest() {
        /*
            压缩报告文件
         */
            Date now = new Date();

        //可以方便地修改日期格式
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String datatime = dateFormat.format(now);
            String sourceFilePath = "reportOutput/";
            String fileName = "Rpindex";

        /*
        发送邮件
         */
            String filePath = "reportOutput/" + fileName + ".html";
            MailUtil mailUtil = new MailUtil();
            subject = "自动化接口测试报告 " + datatime;
            mailHtml = "自动化接口测试报告,详情请见附件,如果打开附件格式有问题,请用chrome浏览器";
            try {
                mailUtil.sendMail(testAddressList, subject, mailHtml, fileName+ ".html", filePath);
            } catch (MessagingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
    }

    public static void main(String[] args) {
        try {
            Properties mailProp = PropertiesFileReader.readProperties(ConfigUtils.mailConfig);
            MailListener mailListener = new MailListener();
           // mailListener.failMailTest();
            String receiveMail = mailProp.getProperty("receiveMail");
            char[] chars = receiveMail.toCharArray();
            System.out.println(chars[0]);

        } catch (AddressException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

