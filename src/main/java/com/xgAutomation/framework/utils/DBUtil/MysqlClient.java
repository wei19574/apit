package com.xgAutomation.framework.utils.DBUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by on 2016/7/19.
 */
public class MysqlClient {
    public String url = "jdbc:mysql://";
    public String name = "com.mysql.jdbc.Driver";

    public Connection conn = null;
    public Statement stmt = null;
    public ResultSet rs = null;

    public MysqlClient(String host, String port, String instanceName, String username, String password) {
        url = url + host + ":" + port + "/" + instanceName + "?" + "user=" + username + "&password=" + password + "&useUnicode=true&characterEncoding=UTF8";
    }
    
    
    
    public ResultSet ExecuteSelectSQL(String sql) {
        try {
            Class.forName(name);//指定连接类型
            conn = DriverManager.getConnection(url);//获取连接
            stmt = conn.createStatement();//准备执行语句
            rs = stmt.executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public int ExecuteSingleUpdateSQL(String sql) {
        int result = 0;
        try {
            Class.forName(name);//指定连接类型
            conn = DriverManager.getConnection(url);//获取连接     
            stmt = conn.createStatement();//准备执行语句
            result = stmt.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void close() {
        try {
            this.conn.close();
            this.stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
