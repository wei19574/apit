package com.xgAutomation.framework.utils.Encrypt;

import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.LogReader.LogService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by MingWei on 2018/3/30
 * Description
 */
public class Sha256 {
    /**
     * 加盐 SHA-256
     * @param salt
     * @param plaintext
     * @return
     */
    public static String encryptInSha256(String salt, String plaintext) {
        // 混合加盐和密码原文
        int a = plaintext.length();
        int b = salt.length();
        String c = plaintext.substring(0, a / 4) + salt.substring(0, b / 2)
                + plaintext.substring(a / 4, a / 2) + salt.substring(b / 2, b)
                + plaintext.substring(a / 2, a);
        plaintext = new String(c);

        try {
            // SHA 加密开始
            // 创建加密对象 并传入加密类型
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            // 传入要加密的字符串
            messageDigest.update(plaintext.getBytes());
            // 得到 byte 类型结果
            byte byteBuffer[] = messageDigest.digest();
            // 將 byte 转换为string
            StringBuffer strHexString = new StringBuffer();
            // 遍历 byte buffer
            for (int i = 0; i < byteBuffer.length; i++) {
                String hex = Integer.toHexString(0xff & byteBuffer[i]);
                if (hex.length() == 1) {
                    strHexString.append('0');
                }
                strHexString.append(hex);
            }
            return strHexString.toString();
        } catch (NoSuchAlgorithmException e) {
            LogService.error(Sha256.class, LogType.REQUEST,"### sha-256 encrypt for [text:{},salt:{}] failure", plaintext, salt, e);
        }

        return null;
    }

    public static void main(String[] args) {
        System.out.println(encryptInSha256("KMkzALm28K",MD5Util.encoderByMd5With32Bit("wm8884824932")));
        System.out.println(encryptInSha256("KMkzALm28K","wm8884824932"));
    }
}
