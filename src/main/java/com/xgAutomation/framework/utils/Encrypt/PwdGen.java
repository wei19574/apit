package com.xgAutomation.framework.utils.Encrypt;

import java.util.Random;

/**
 * Created by J on 2016/7/20.
 */
public class PwdGen {
    private static char[] getChar(){
        char[] passwordLit = new char[62];
        char fword = 'A';
        char mword = 'a';
        char bword = '0';
        for (int i = 0; i < 62; i++) {
            if (i < 26) {
                passwordLit[i] = fword;
                fword++;
            }else if(i<52){
                passwordLit[i] = mword;
                mword++;
            }else{
                passwordLit[i] = bword;
                bword++;
            }//方法的抽取，按功能
        }
        return passwordLit;
    }

    public static String getRandomPwd(int number){
        StringBuffer sb = new StringBuffer();
        char[] r = getChar();
        Random rr = new Random();
        char[] pw= new char[6];
        for(int i=0;i<number;i++){
            int num = rr.nextInt(62);
            pw[i]=r[num];
            sb.append(pw[i]);
        }
        return sb.toString().toLowerCase();
    }
}
