package com.xgAutomation.framework.utils;/**
 * Created by Administrator on 2017/3/7.
 */

import com.alibaba.fastjson.JSON;
import com.xgAutomation.dataPrepare.sinotrans.Login;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.request.tokooHeader.SinoTransSellerTokenHeaderInterceptor;
import com.xgAutomation.framework.testlink.ReplaceParamUtil;
import com.xgAutomation.framework.utils.Encrypt.Base64;
import com.xgAutomation.framework.utils.Encrypt.Base64Util;
import com.xgAutomation.framework.utils.LogReader.LogService;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.*;

/**
 * 地址选择工具类
 *
 * @author cyl
 * @create 2017-03-07 11:33
 **/
public class ConfigUtils {

    public static final String config = "config.properties";
    
    public static final String makeInterfaceTestcase = "makeInterfaceTestcase.properties";

	public static final String defaultURL = "defaultURL.properties";

	public static final String mailConfig = "mailConfig.properties";

    public static final String format = "json";
    	
    public static final String MD5 = "MD5";
    

    private ConfigUtils(){}


    //Testlink格式处理

	public static String TestlinkSolvedString(String s){
    	return s.replaceAll("<p>","").replaceAll("</p>","").replaceAll("&quot;","\"")
				.replaceAll("&ldquo;","\"").replaceAll("&rdquo;","\"").replaceAll("</div>","")
				.replaceAll("<div>","").replaceAll("&nbsp;","").replaceAll("&amp;","&");
	}

	//检查Testlink 传入的json里面的参数是否有空,以及进行登录

	public static String checkJson(OkHttpClient okHttpClient,RequestFactory re, String json) throws FrameworkException,IOException{
    	if (json == null || json.equals("")){
			LogService.error(ConfigUtils.class, LogType.BIZ,"checkJson的入参json为空");
			throw new FrameworkException("传入的json数据为空");
		}
		JSONObject jsonObject = JSONObject.fromObject(json);
		Map<String,String> paramsMap = new HashMap<>();
		Map<String,Object> mp = (Map<String,Object>) JSON.parseObject(json);
		for (String key : mp.keySet()) {
			String value = String.valueOf(mp.get(key));
			if(value.startsWith("$")){
				paramsMap.put(key,value);
			}
		}

		if(paramsMap.size() > 0){
			for(String key : paramsMap.keySet()){
				String handledParam = ReplaceParamUtil.paramHandle(paramsMap.get(key));
				jsonObject.put(key,handledParam);
			}
		}
		//小二端登录
		if (mp.get("usernameLogin") != null ){
				String userName = (String)mp.get("usernameLogin");
				String passWord = (String)mp.get("passwordLogin");
				jsonObject.discard("usernameLogin");
				jsonObject.discard("passwordLogin");
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("email",userName);
				//base64加密密码
				jsonObject1.put("password", Base64Util.encode(passWord.getBytes("UTF-8")));
				String accessToken = Login.login_waiter(okHttpClient,re,jsonObject1.toString());
				jsonObject.put("accessToken",accessToken);


			}
		//买家端登录
			if (mp.get("manNameLogin") != null){
				jsonObject.discard("manNameLogin");
				jsonObject.discard("manPassword");
				String userName = (String)mp.get("manNameLogin");
				String passWord = (String)mp.get("manPassword");
				Map<String,String> map = new HashMap<>();
				map.put("email",userName);
				map.put("password",passWord);

				String accessToken = Login.login(okHttpClient,re,map);
				if (jsonObject.containsKey("tempAccessToken")){
					jsonObject.put("tempAccessToken", accessToken);
				}else {
					jsonObject.put("accessToken", accessToken);
				}
			}
		//卖家端登录
		if (mp.get("sellerLogin") != null){
			jsonObject.discard("sellerLogin");
			jsonObject.discard("sellerPassword");
			String userName = (String)mp.get("sellerLogin");
			String passWord = (String)mp.get("sellerPassword");
			Map<String,String> map = new HashMap<>();
			map.put("email",userName);
			map.put("password",passWord);
			String accessToken = Login.login_seller(okHttpClient,re,map);

			jsonObject.put("accessToken",accessToken);
		}
    	return jsonObject.toString();
	}




	/**
	 * 替换json里面的部分数据
	 * @param map
	 * @param json
	 * @return
	 */
	public static String replaceJson(Map<String,Object> map,String json){
		JSONObject jsonObject = JSONObject.fromObject(json);
		for (String key:map.keySet()){
			if (jsonObject.containsKey(key)){
				jsonObject.put(key,map.get(key));
			}
		}
		return jsonObject.toString();
	}

	public static String StringReplaceJson(Map<String,Object> map,String json){
		JSONObject jsonObject = JSONObject.fromObject(json);
		for (String key:map.keySet()){
			if (jsonObject.containsKey(key)){
				jsonObject.put(key,map.get(key));
			}
		}
		return jsonObject.toString();
	}



}
