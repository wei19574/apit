package com.xgAutomation.framework.request.tokooHeader;


import com.xgAutomation.dataPrepare.sinotrans.Login;
import com.xgAutomation.dataPrepare.sinotrans.RoleType;
import com.xgAutomation.dataPrepare.sinotrans.Token;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.LogReader.LogService;
import okhttp3.Request;
import okhttp3.Response;
import sun.rmi.runtime.Log;

import java.io.IOException;

public class SinoTransSellerTokenHeaderInterceptor implements okhttp3.Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        //token需要赋值
        String token = "fff41810e7a342c7809e4cc9448e71c3";
//        try {
//            token = Token.getToken("xiaoyan.zhang@xinguang.com","123456", RoleType.SELLER);
//        } catch (FrameworkException e) {
//            LogService.debug(SinoTransSellerTokenHeaderInterceptor.class, LogType.Intercept,"accessToken获取有问题");
//            e.printStackTrace();
//        }
        if (token == null) {
            LogService.debug(SinoTransSellerTokenHeaderInterceptor.class, LogType.Intercept, "token为null");
        }

        LogService.debug(SinoTransSellerTokenHeaderInterceptor.class, LogType.Intercept, "accessToken 不为null");
        Request originaRequest = chain.request();

        Request updataRequest = originaRequest.newBuilder()
                .header("accessToken", token)
                .build();
        return chain.proceed(updataRequest);
    }
}
