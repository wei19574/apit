package com.xgAutomation.framework.request.tokooHeader;

import com.xgAutomation.dataPrepare.sinotrans.RoleType;
import com.xgAutomation.dataPrepare.sinotrans.Token;
import com.xgAutomation.framework.exception.FrameworkException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by MingWei on 2018/4/8
 * Description
 */
public class SinoTransBuyerTokenHeaderInterceptor implements Interceptor{
    @Override
    public Response intercept(Chain chain) throws IOException {

        //token需要赋值
        String token = null;
        try {
            token = Token.getToken("","", RoleType.BUYER);
        } catch (FrameworkException e) {
            e.printStackTrace();
        }

        if(null != token) {
            Request originaRequest = chain.request();

            Request updataRequest = originaRequest.newBuilder()
                    .header("Authorization", token)
                    .header("cooklie","lang=en-US; ut="+token)
                    .build();
            return chain.proceed(updataRequest);
        }else{
            throw new NullPointerException("token为null");
        }
    }
}

