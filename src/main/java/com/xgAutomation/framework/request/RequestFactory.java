package com.xgAutomation.framework.request;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.LogReader.LogService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import okhttp3.*;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author
 * @Date 2016-07-11
 */
public class RequestFactory {

    private static Properties prop = null;
    private static String webPropertyAdds = "header/webHeader.properties";
    private static String htmlPropertyAdds = "header/htmlUrlHeader.properties";
    private static String appPropertyAdds = "header/appHeader.properties";
    public static String body;

    //Post
    //Map请求参数
    public com.xgAutomation.framework.meta.Response okHttpPost_Json(OkHttpClient client, String url, Map<String, Object> bizparams, String projectHost) throws FrameworkException {

        String parameters;

        if (bizparams == null) {
            throw new FrameworkException("传入参数为空");
        }

        //传入map参数转换json
        JSONObject json = JSONObject.fromObject(bizparams);
        parameters = json.toString();


        //拼接url
        if (null != url) {
            if (!url.startsWith("http")) {
                url = urlConcat(url, projectHost);
            }
        } else {
            throw new NullPointerException("url不能为空");
        }
        LogService.debug(RequestFactory.class, LogType.REQUEST, "okHttpPost_Json()输入的参数为：" +
                "\r\nclient = " + client +
                "\r\nurl : " + url +
                "\r\nbizparams = " + bizparams.toString() +
                "\r\nprojectHost : " + projectHost);


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        //构造http body
        RequestBody body = RequestBody.create(JSON, parameters);
        Request request = new Request.Builder()
                //.header("mac","11-22-33-44-55-66-77-88")
                .url(url)
                .post(body)
                .build();
        //获取response
        okhttp3.Response okResponse = null;
        com.xgAutomation.framework.meta.Response response = null;
        try {
            okResponse = client.newCall(request).execute();
            LogService.debug(RequestFactory.class, LogType.REQUEST, "okhttp的response为：" + okResponse);
            if (null != okResponse || okResponse.code() == 200) {
                String resHeader = okResponse.headers().toString();
                String Resbody = okResponse.body().string();
                LogService.debug(RequestFactory.class, LogType.REQUEST, "resbody的值为：" + Resbody);
                JSONObject jsonObject = JSONObject.fromObject(Resbody);
                int code = (Integer) (jsonObject.get("code"));
                //okhttp response 转换 Response
                response = new com.xgAutomation.framework.meta.Response(code, url, resHeader, parameters, Resbody);
                LogService.info(RequestFactory.class, LogType.REQUEST, "-----Post请求结果-----" +
                        "\r\ncode = " + code +
                        "\r\nurl : " + url +
                        "\r\nparam : " + parameters +
                        "\r\nresponse = " + Resbody);

            } else {
                throw new FrameworkException("返回的response为空");
            }
        } catch (IOException e) {
            LogService.error(RequestFactory.class, LogType.REQUEST, "异常：" + e);
        }
        return response;
    }

    //Post
    //TestlinkApi
    public com.xgAutomation.framework.meta.Response okHttp_Post_Testlink(OkHttpClient client, String url, String json) throws FrameworkException {

        if (json == null) {
            throw new NullPointerException("传入json参数为null");
        }

        if (url == null) {
            throw new NullPointerException("传入的url参数为null");
        }

        String accessToken = "";
        JSONObject jsonObject = JSONObject.fromObject(json);
        if (null != jsonObject.get("accessToken") && !("").equals(jsonObject.get("accessToken"))) {
            accessToken = String.valueOf(jsonObject.get("accessToken"));
            jsonObject.discard("accessToken");
        }

        LogService.debug(RequestFactory.class, LogType.REQUEST, "okHttp_Post_Testlink()输入的参数为：" +
                "\r\nurl : " + url +
                "\r\njson = " + json.toString());

        MediaType jsonMediaType = MediaType.parse("application/json; charset=utf-8");
        //构造http body
        RequestBody body = RequestBody.create(jsonMediaType, jsonObject.toString());
        Request request;
        if (!accessToken.equals("")) {
            request = new Request.Builder()
                    .addHeader("accessToken", accessToken)
                    .url(url)
                    .post(body)
                    .build();
        } else {
            request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
        }
        //获取response
        okhttp3.Response okResponse = null;
        com.xgAutomation.framework.meta.Response response = null;
        try {
            okResponse = client.newCall(request).execute();
            LogService.debug(RequestFactory.class, LogType.REQUEST, "okhttp的response为：" + okResponse);
            if (null != okResponse || okResponse.code() == 200) {
                String resHeader = okResponse.headers().toString();
                String Resbody = okResponse.body().string();
                LogService.debug(RequestFactory.class, LogType.REQUEST, "resbody的值为：" + Resbody);
                JSONObject jsonObject1 = JSONObject.fromObject(Resbody);
                int code = (Integer) (jsonObject1.get("retCode"));
                //okhttp response 转换 Response
                response = new com.xgAutomation.framework.meta.Response(code, url, resHeader, json, Resbody);
                LogService.info(RequestFactory.class, LogType.REQUEST, "-----Post请求结果-----" +
                        "\r\ncode = " + code +
                        "\r\nurl : " + url +
                        "\r\nparam : " + json +
                        "\r\nresponse = " + Resbody);

            } else {
                LogService.error(RequestFactory.class, LogType.REQUEST, "返回的response不正确，响应值为" +
                        "\r\nstatus = " + okResponse.code() +
                        "\r\nurl : " + url +
                        "\r\nparam : " + jsonObject.toString() +
                        "\r\nmessage : " + okResponse.message());
                throw new FrameworkException("返回的response为空");
            }
        } catch (IOException e) {
            LogService.error(RequestFactory.class, LogType.REQUEST, "异常：" + e);
        }
        return response;
    }


    //FORM表单格式请求
    public com.xgAutomation.framework.meta.Response okHttpPost_Form(OkHttpClient client, String url, String jsonBody) throws FrameworkException {
        LogService.debug(RequestFactory.class, LogType.REQUEST, "-----Request入参-----" +
                "\r\nurl = " + url +
                "\r\njsonBody = " + jsonBody);
        if (jsonBody.equals("")) {
            throw new IllegalArgumentException("json为不正确输入");
        }
        String accessToken = "";
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        if (jsonObject.get("accessToken") != null && !("").equals(jsonObject.get("accessToken"))) {
            accessToken = String.valueOf(jsonObject.get("accessToken"));
            jsonObject.discard("accessToken");
        }
        Map<String, Object> formMap = jsonObject;
        RequestBody requestBody;
        FormBody.Builder builder = new FormBody.Builder();
        for (String s : formMap.keySet()) {
            builder.add(s, String.valueOf(formMap.get(s)));
        }
        requestBody = builder.build();
        Request request;
        //构造http body
        if (accessToken != "") {
            request = new Request.Builder()
                    .addHeader("accessToken", accessToken)
                    .url(url)
                    .post(requestBody)
                    .build();
        } else {
            request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();
        }
        //获取response
        okhttp3.Response okResponse = null;
        com.xgAutomation.framework.meta.Response response = null;
        String Resbody = "";
        try {
            okResponse = client.newCall(request).execute();

            if (null != okResponse && okResponse.code() == 200) {
                JSONObject jsonObject1 = new JSONObject();
                String resHeader = okResponse.headers().toString();
                Resbody = okResponse.body().string();
                if (!Resbody.equals(null) && !Resbody.equals("")) {
                    jsonObject1 = JSONObject.fromObject(Resbody);
                } else {
                    throw new FrameworkException("返回的response为空");
                }
                LogService.debug(RequestFactory.class, LogType.REQUEST, "请求返回的Resbody为：" + Resbody);

                //okhttp response 转换 Response
                int code = (Integer) (jsonObject1.get("retCode"));
                response = new com.xgAutomation.framework.meta.Response(code, url, resHeader, "" + requestBody, Resbody);
                LogService.info(RequestFactory.class, LogType.REQUEST, "-----Post请求结果-----" +
                        "\r\ncode = " + code +
                        "\r\nurl : " + url +
                        "\r\nparam : " + formMap.toString() +
                        "\r\nresponse = " + Resbody);
                if(code != 1){
                    throw new FrameworkException(
                            "\r\n"+Resbody +
                    "\r\nurl为：" + url+
                    "\r\nparam:" + formMap.toString());
                }
            } else {
                LogService.error(RequestFactory.class, LogType.REQUEST, "返回的response状态码不正确，响应值为" +
                        "\r\nstatus = " + okResponse.code() +
                        "\r\nurl : " + url +
                        "\r\nparam : " + formMap.toString() +
                        "\r\nmessage : " + okResponse.message());
                throw new FrameworkException("返回的response为空");
            }
        } catch (IOException e) {
            LogService.error(RequestFactory.class, LogType.REQUEST, "异常：" + e);
        }
        return response;
    }

    public com.xgAutomation.framework.meta.Response okHttpPost_Form_paramAccessToken(OkHttpClient client, String url, String jsonBody) throws FrameworkException {
        LogService.debug(RequestFactory.class, LogType.REQUEST, "-----Request入参-----" +
                "\r\nurl = " + url +
                "\r\njsonBody = " + jsonBody);
        if (jsonBody.equals("")) {
            throw new FrameworkException("json为不正确输入");
        }
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        Map<String, Object> formMap = jsonObject;
        RequestBody requestBody;
        FormBody.Builder builder = new FormBody.Builder();
        for (String s : formMap.keySet()) {
            builder.add(s, String.valueOf(formMap.get(s)));
        }
        requestBody = builder.build();
        //构造http body
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        //获取response
        okhttp3.Response okResponse = null;
        com.xgAutomation.framework.meta.Response response = null;
        try {
            okResponse = client.newCall(request).execute();
            if (null != okResponse && okResponse.code() == 200) {
                JSONObject jsonObject1 = new JSONObject();
                String resHeader = okResponse.headers().toString();
                String Resbody = okResponse.body().string();
                if (!Resbody.equals(null) && !Resbody.equals("")) {
                    jsonObject1 = JSONObject.fromObject(Resbody);
                } else {
                    throw new NullPointerException("返回的response为空");
                }
                LogService.debug(RequestFactory.class, LogType.REQUEST, "请求返回的Resbody为：" + Resbody);

                //okhttp response 转换 Response
                int code = (Integer) (jsonObject1.get("retCode"));
                response = new com.xgAutomation.framework.meta.Response(code, url, resHeader, "" + requestBody, Resbody);
                LogService.info(RequestFactory.class, LogType.REQUEST, "-----Post请求结果-----" +
                        "\r\ncode = " + code +
                        "\r\nurl : " + url +
                        "\r\nparam : " + jsonBody +
                        "\r\nresponse = " + Resbody);
            } else {
                throw new FrameworkException("返回的response为空，code为"+okResponse.code() + " url为："+url);
            }
        } catch (IOException e) {
            LogService.error(RequestFactory.class, LogType.REQUEST, "异常：" + e);
        }
        return response;
    }

    public com.xgAutomation.framework.meta.Response okHttpPost_JsonArray(OkHttpClient client, String url, JSONArray jsonArray, String projectHost) throws FrameworkException {

        String parameters;

        if (null == jsonArray) {
            throw new NullPointerException("传入参数为空");
        }

        //传入map参数转换json
        parameters = jsonArray.toString();


        //拼接url
        if (null != url) {
            if (!url.startsWith("http")) {
                url = urlConcat(url, projectHost);
            }
        } else {
            throw new FrameworkException("url不能为空");
        }
        LogService.debug(RequestFactory.class, LogType.REQUEST, "okHttpPost_Json()输入的参数为：" +
                "\r\nclient = " + client +
                "\r\nurl : " + url +
                "\r\nbizparams = " + jsonArray.toString() +
                "\r\nprojectHost : " + projectHost);


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        //构造http body
        RequestBody body = RequestBody.create(JSON, parameters);
        Request request = new Request.Builder()
                //.header("mac","11-22-33-44-55-66-77-88")
                .url(url)
                .post(body)
                .build();
        //获取response
        okhttp3.Response okResponse = null;
        com.xgAutomation.framework.meta.Response response = null;
        try {
            okResponse = client.newCall(request).execute();
            LogService.debug(RequestFactory.class, LogType.REQUEST, "okhttp的response为：" + okResponse);
            if (null != okResponse || okResponse.code() == 200) {
                String resHeader = okResponse.headers().toString();
                String Resbody = okResponse.body().string();
                JSONObject jsonObject = JSONObject.fromObject(Resbody);
                int code = (Integer) (jsonObject.get("code"));
                //okhttp response 转换 Response
                response = new com.xgAutomation.framework.meta.Response(code, url, resHeader, parameters, Resbody);
                LogService.info(RequestFactory.class, LogType.REQUEST, "-----Post请求结果-----" +
                        "\r\ncode = " + code +
                        "\r\nurl : " + url +
                        "\r\nparam : " + parameters +
                        "\r\nresponse = " + Resbody);

            } else {
                throw new FrameworkException("返回的response为空");
            }
        } catch (IOException e) {
            LogService.error(RequestFactory.class, LogType.REQUEST, "异常：" + e);
        }
        return response;
    }

    //GET

    public static com.xgAutomation.framework.meta.Response okhttp_get(OkHttpClient client, String url,String json) throws IOException, FrameworkException {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
        if (!json.isEmpty()){
            JSONObject jsonObject = JSONObject.fromObject(json);
            for (Object key : jsonObject.keySet()){
                urlBuilder.setQueryParameter(key.toString(),jsonObject.getString(key.toString()));
            }
        }


        Request request = new Request.Builder()
                .url(urlBuilder.build())
                .get()
                .build();
        //获取response
        okhttp3.Response okResponse = null;
        com.xgAutomation.framework.meta.Response response = null;
        String Resbody = "";
        try {
            okResponse = client.newCall(request).execute();

            if (null != okResponse && okResponse.code() == 200) {
                JSONObject jsonObject1 = new JSONObject();
                String resHeader = okResponse.headers().toString();
                Resbody = okResponse.body().string();
                if (!Resbody.equals(null) && !Resbody.equals("")) {
                    jsonObject1 = JSONObject.fromObject(Resbody);
                } else {
                    throw new FrameworkException("返回的response为空");
                }
                LogService.debug(RequestFactory.class, LogType.REQUEST, "请求返回的Resbody为：" + Resbody);

                //okhttp response 转换 Response
                int code = (Integer) (jsonObject1.get("retCode"));
                response = new com.xgAutomation.framework.meta.Response(code,urlBuilder.toString(),"",Resbody);
                LogService.info(RequestFactory.class, LogType.REQUEST, "-----Post请求结果-----" +
                        "\r\ncode = " + code +
                        "\r\nurl : " + url +
                        "\r\nresponse = " + Resbody);
            } else {
                LogService.error(RequestFactory.class, LogType.REQUEST, "返回的response状态码不正确，响应值为" +
                        "\r\nstatus = " + okResponse.code() +
                        "\r\nurl : " + url +
                        "\r\nmessage : " + okResponse.message());
                throw new FrameworkException("返回的response为空");
            }
        } catch (IOException e) {
            LogService.error(RequestFactory.class, LogType.REQUEST, "异常：" + e);
        }
        return response;
    }

    //url拼接
    public static String urlConcat(String url, String projectHost) {
        url = projectHost + url;
        LogService.info(RequestFactory.class, LogType.REQUEST, "url is: " + url);
        return url;
    }


}
