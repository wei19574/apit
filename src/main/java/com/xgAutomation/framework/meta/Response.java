package com.xgAutomation.framework.meta;

import org.apache.http.Header;

import java.util.Arrays;

/**
 * @author weim
 * @Date 2017-08-28
 */
public class Response {
    int code;
    String url;
    String param;
    String body;
    Header[] header;
    String location;
    String headers;


    public Response(int code, String url, String body, Header[] header, String location) {
        super();
        this.code = code;
        this.url = url;
        this.body = body;
        this.header = header;
        this.location = location;
    }

    public Response(int code, String url, String param, String body, Header[] header, String location) {
        super();
        this.code = code;
        this.url = url;
        this.param = param;
        this.body = body;
        this.header = header;
        this.location = location;
    }

    public Response(int code, String url, String param, String body) {
        super();
        this.code = code;
        this.url = url;
        this.param = param;
        this.body = body;
    }

    public Response(int code, String url, String headers,String param, String body) {
        super();
        this.code = code;
        this.url = url;
        this.param = param;
        this.body = body;
        this.headers = headers;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Header[] getHeader() {
        return header;
    }

    public void setHeader(Header[] header) {
        this.header = header;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "Response{" +
                "code=" + code +
                ", url='" + url + '\'' +
                ", param='" + param + '\'' +
                ", body='" + body + '\'' +
                ", header=" + Arrays.toString(header) +
                ", location='" + location + '\'' +
                ", headers='" + headers + '\'' +
                '}';
    }
}
