package com.xgAutomation.framework.meta;


import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;

import java.util.Properties;

public class ProjectHost {
	//项目配置
	static Properties TokoonderdilProp = PropertiesFileReader.readProperties("Tokoonderdil/tokoonderdil.properties");
	//sjyx
//	public final static String SJYX = sjyx.getProperty("sjyxUtil");
//	public final static String SJYXPIS = sjyx.getProperty("sjyxpis");
	//tuchao
	public final static String Tokoonderdil_Supplier= TokoonderdilProp.getProperty("Tokoonderdil_Supplier");
	public final static String Tokoonderdil_Buyer= TokoonderdilProp.getProperty("Tokoonderdil_Buyer");
	public final static String Tokoonderdil_Backend= TokoonderdilProp.getProperty("Tokoonderdil_Backend");
	public final static String Tokoonderdil_Search= TokoonderdilProp.getProperty("Tokoonderdil_Search");
}
