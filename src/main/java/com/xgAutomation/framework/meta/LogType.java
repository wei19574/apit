package com.xgAutomation.framework.meta;

public class LogType {

    /**业务类型日志*/
    public static final String BIZ = "BIZ";

    /**request调用类型日志*/
    public static final String REQUEST  = "REQUEST";

    /**dubbo调用*/
    public static final String DUBBOINVOKE = "DUBBOINVOKE";

    /**断言*/
    public static final String Assert = "Assert";

    /**TestlinkApi*/
    public static final String TestlinkApi = "TestlinkApi";

    /**DataProvider*/
    public static final String DataProvider = "DataProvider";

    /**初始化*/
    public static final String Init = "Init";

    /**拦截器*/
    public static final String Intercept = "intercept";

}
