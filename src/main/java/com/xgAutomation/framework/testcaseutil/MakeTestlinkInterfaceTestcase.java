package com.xgAutomation.framework.testlink;

import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.CsvReader.csvUtils;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import com.xgAutomation.framework.utils.ConfigUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by MingWei on 2018/6/12
 * Description
 */
public class MakeTestlinkInterfaceTestcase {
        private Configuration config;
        private static String SourcetemplateFile;
        public static String csv;
        Properties prop;
        //初始化数据
        @BeforeTest
        public void init() throws IOException {
            config = new Configuration();
            config.setDirectoryForTemplateLoading(new File("template"));
            prop = PropertiesFileReader.readProperties(ConfigUtils.makeInterfaceTestcase);

            //代码模板路径
            SourcetemplateFile = prop.getProperty("TestlinkTcFtl");
            //csv模板路径
            csv = prop.getProperty("TestlinkTcCsv");

        }
        @Test(dataProvider = "dataPro",testName = "make")
        public void make(String testcaseDir,String interfaceName,
                         String isRun) throws IOException{
            if(isRun.equals("Y")){
                LogService.info(com.xgAutomation.framework.utils.makeInterfaceTestcase.makeInterfaceTestcaseUtil.class, LogType.BIZ,"开始创建"+interfaceName+"接口");
                Map<String,String> mp = new HashMap<>();
                String packageDir = ("com/xgAutomation/" + testcaseDir).replace("/",".");
                String lowinterfaceMethod = interfaceName.substring(0,1).toLowerCase() + interfaceName.substring(1,interfaceName.length());
                String upInterfaceName = interfaceName.substring(0,1).toUpperCase() + interfaceName.substring(1,interfaceName.length());

                mp.put("interfaceMethod", lowinterfaceMethod);
                mp.put("interfaceName", upInterfaceName);
                mp.put("packageDir", packageDir);

                String sourceFileName = upInterfaceName + ".java";
                String sourceSavaPath = "./src/main/java/com/xgAutomation/" + testcaseDir + "/";


                Template sourceTemplate = config.getTemplate(SourcetemplateFile);
                com.xgAutomation.framework.utils.makeInterfaceTestcase.makeInterfaceTestcaseUtil.bulid(mp, sourceSavaPath, sourceFileName, sourceTemplate);

            }else if(isRun.equals("N")){
                LogService.info(com.xgAutomation.framework.utils.makeInterfaceTestcase.makeInterfaceTestcaseUtil.class, LogType.BIZ,"isRun为N，无须创建"+interfaceName+"接口");
            }else{
                System.out.println("请检查isRun的值是否正确");
            }
        }

        public static void bulid(Map<String,String> mp,String savePath,String fileName,Template template) throws IOException {

            String file_name = savePath + fileName;
            LogService.info(MakeTestlinkInterfaceTestcase.class,LogType.BIZ,"file_name   " + file_name);
            File directory = new File(savePath);
            if(!directory.exists()){
                directory.mkdirs();
            }
            File file = new File(savePath);
            if (file.isFile()) {
                throw new AssertionError(fileName + "文件已经存在，请删除已存在文件后重新运行脚本");
            }else{
                file.createNewFile();
            }
            try {
                Writer wr = new OutputStreamWriter(new FileOutputStream(file_name));
                template.process(mp,wr);
                wr.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (TemplateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @DataProvider
        public Object[][] dataPro(){
            return csvUtils.csvReader(csv);
    }
}
