package com.xgAutomation.framework.testlink;


/**
 * Created by liuqq on 2018/6/7.
 */
public enum CustomFieldsParams {
    URL("url");//url

    private String value;

    CustomFieldsParams(String value) {
        this.value = value;
    }


    /**
     * Print param value.
     * @return param value
     */
    public String toString() {
        return this.value;
    }

}
