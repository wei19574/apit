package com.xgAutomation.framework.testlink;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.*;
import br.eti.kinoshita.testlinkjavaapi.model.*;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created by MingWei on 2018/6/4
 * Description
 */
public class TestlinkApiUtil {


    private TestlinkApiUtil(){}

    public static final Properties configProp = PropertiesFileReader.readProperties(ConfigUtils.config);
    /**
     * 连接Testlink
     * @param devKey （个人秘钥）
     * @return TestLinkAPI
     */
    public static TestLinkAPI ConnectTL(String devKey) throws FrameworkException {
        //读取config配置文件
        String url = PropertiesFileReader
                                .readProperties(ConfigUtils.config)
                                .getProperty("TestlinkUrl");

        if(url == null){
                throw new FrameworkException("Url 获取失败，请检查配置文件"); }

        LogService.debug(TestlinkApiUtil.class, LogType.REQUEST,"Testlink的连接Url为： " + url);

        TestLinkAPI api = null;

        URL testlinkUrl = null;

        //生成Url
        try {
            testlinkUrl = new URL(url);
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
            LogService.error(mue.getClass(),LogType.REQUEST,mue.getMessage());
        }

        api = new TestLinkAPI(testlinkUrl,devKey);

        return api;
    }

    /**
     * 根据项目名字获取项目ID
     * @param api 连接获取的api
     * @param projectName  项目名称
     * @return  项目ID
     * @throws FrameworkException
     */
    public static Integer getProjectIdByProjectName(TestLinkAPI api,String projectName) throws FrameworkException {

        int projectId = -1;

        if(projectName == ""||projectName == null){throw new IllegalArgumentException("projectName参数输入非法");}

        TestProject testProject = api.getTestProjectByName(projectName);
//        for(TestProject tp : tps){
//            if(tp.getName().equals(projectName)){
//                projectId = tp.getId();
//            }else{
//                System.out.println("没有符合名称的项目");
//            }
//        }

        if(testProject == null){throw new FrameworkException("没有找到符合名字的项目");}

        projectId = testProject.getId();

        return projectId;
    }

    /**
     * 获得特定项目下所有测试集
     * @param api
     * @param projectId 项目ID
     * @return  测试集数组
     * @throws FrameworkException
     */
    public static TestSuite[] getTestsuit(TestLinkAPI api,int projectId) throws FrameworkException {
        if(api == null){throw new FrameworkException("获取到api为空，请检查Testlink连接");}

        TestSuite[] testSuites = api.getFirstLevelTestSuitesForTestProject(projectId);

        return testSuites;
    }

    /**
     * 根据测试计划获取用例
     * @param api
     * @param testPlanId 测试计划id
     * @param buildId 计划版本id
     * @return
     * @throws FrameworkException
     */
    public static TestCase[] getTestCasesForPlan(TestLinkAPI api,Integer testPlanId,Integer buildId)throws FrameworkException{
        if(api == null){
            throw new FrameworkException("获取到api为空，请检查Testlink连接");
        }
        if(testPlanId == null){
            throw new FrameworkException("获取到testPlanId为空，请检查测试计划是否存在");
        }
        if(buildId == null){
            throw new FrameworkException("获取到buildId为空，请检查测试计划版本是否存在");
        }

        TestCase[] testCases = api.getTestCasesForTestPlan(testPlanId,null,buildId,null,"",true,null,null, ExecutionType.AUTOMATED,true, TestCaseDetails.FULL);
        if (testCases == null){
            throw new FrameworkException("没有找到测试计划这个版本的用例");
        }
        return testCases;

    }

    /**
     * 获取用例列表
     * @param api
     * @param testPlanId 测试计划id
     * @param buildId 计划版本id
     * @return 测试用例列表
     * [0] testcaseID
     * [1] CustomFields
     * [2] stepchar
     * [3] summary
     * [4] preconditions
     * [5] expectedResult
     *
     */
    public static List<Object[]> getTestCaseList(TestLinkAPI api,Integer testPlanId,Integer buildId,Integer projectId)throws FrameworkException{
        //获取计划下测试用例
        TestCase[] testCases = getTestCasesForPlan(api,testPlanId,buildId);
        List<Object[]> mapList = new ArrayList<>();

        for (int i = 0;i<testCases.length;i++){
            //获取测试用例信息
            TestCase testCase = testCases[i];
            TestCase testCase1 = api.getTestCase(testCase.getId(),null,testCase.getVersion());
            //获取用例自定义字段
            //Map<String,String> map = getCustomFields(api,testCase1,projectId);
            CustomField urlCustomFiled = api.getTestCaseCustomFieldDesignValue(testCase.getId(),null,1,projectId,"url",null);
            String url = urlCustomFiled.getValue();
            Object[] scase = new Object[6];
            if (testCase1.getId()!=null){

                scase[3] = testCase1.getId();
            }
            scase[1] = url;

            //获取测试步骤
            if (testCase1.getSteps()!=null){
                TestCaseStep step = testCase1.getSteps().get(0);
                String stepchar = ConfigUtils.TestlinkSolvedString(step.getActions());
                scase[2] = stepchar.replace("，",",");
            }

            //获取测试用例摘要
            if (testCase1.getSummary()!=null){
                String summary = ConfigUtils.TestlinkSolvedString(testCase1.getSummary());
                scase[0] = summary;
            }
            //获取测试用例前提
            String preconditions = ConfigUtils.TestlinkSolvedString(testCase1.getPreconditions());
            if(preconditions != null && !preconditions.equals("")) {
                String solvedPre = preconditions.substring(preconditions.indexOf("c"), preconditions.length() - 1);
                scase[4] = solvedPre;
            }
            //用例预期结果
            scase[5] = StringUtils.removeBackspace(ConfigUtils.TestlinkSolvedString(testCase1.getSteps().get(0).getExpectedResults()));
            mapList.add(scase);
        }

        return mapList;
    }

    /**
     * 获取自定义字段
     * @param api
     * @param testCase 测试用例
     * @param projectId 项目id
     * @return
     * @throws FrameworkException
     */
    public static Map<String,String> getCustomFields(TestLinkAPI api,TestCase testCase,Integer projectId)throws FrameworkException{
        if (testCase == null){
            throw new FrameworkException("测试用例为空，请检查");
        }
        if (projectId == null){
            throw new FrameworkException("项目为空，请检查");
        }

        Map<String,String> map = new HashMap<>();
        for(CustomFieldsParams e : CustomFieldsParams.values()){

            CustomField customField = api.getTestCaseCustomFieldDesignValue(testCase.getId(),null,testCase.getVersion(),projectId,e.toString(),ResponseDetails.FULL);
            if(customField.getValue() != null){
                map.put(e.toString(),customField.getValue());
            }else {
                map.put(e.toString(),null);
            }
        }
        return map;
    }

    /**
     * 获取该计划下的这个名称的版本
     * @param api
     * @param testPlanId 计划id
     * @param buildName 版本名称
     * @return
     * @throws FrameworkException
     */

    public static Integer getBuildId(TestLinkAPI api,Integer testPlanId,String buildName)throws FrameworkException{
        Build[] builds = api.getBuildsForTestPlan(testPlanId);
        if (builds == null){
            throw new FrameworkException("该计划下面没有版本");
        }
        for (int i = 0; i < builds.length; i++){
            Build build = builds[i];
            if (build.getName().equals(buildName)){
                return build.getId();
            }
        }
        return null;
    }


    public static String getCustomField(TestLinkAPI api,TestCase testCase,String customfieldName)throws FrameworkException{
        if (testCase == null){
            throw new FrameworkException("测试用例为空，请检查");
        }
        if (customfieldName == null){
            throw new FrameworkException("自定义字段为空，请检查");
        }
        int projectId = TestlinkApiUtil.getProjectIdByProjectName(api,configProp.getProperty("ProjectName"));
        CustomField customField = api.getTestCaseCustomFieldDesignValue(testCase.getId(),null,testCase.getVersion(),projectId,customfieldName,ResponseDetails.FULL);
        LogService.info(TestlinkApiUtil.class,LogType.TestlinkApi,"customField的值为" + customField.getValue());
        return customField.getValue();
    }

    public static void deleteTestcaseStep() throws FrameworkException {
        Properties configProp = PropertiesFileReader.readProperties(ConfigUtils.config);
        TestLinkAPI api = TestlinkApiUtil.ConnectTL(configProp.getProperty("DevKey"));
        System.out.println(configProp.getProperty("DevKey"));
        int testPlanId = api.getTestPlanByName(configProp.getProperty("PlanName"),configProp.getProperty("ProjectName")).getId();
        int buildId = getBuildId(api, testPlanId, configProp.getProperty("BuildName"));
        TestCase[] testCases =api.getTestCasesForTestPlan(testPlanId,null,buildId,null,"",true,null,null, ExecutionType.MANUAL,true, TestCaseDetails.FULL);

        TestCase testCase = testCases[0];
        TestCase testCase1 = api.getTestCase(testCase.getId(),null,testCase.getVersion());
        api.deleteTestCaseSteps(testCase1.getFullExternalId(),testCase1.getVersion(),testCase1.getSteps());
        System.out.println("success");
    }

    public static void main(String[] args) {
        //System.out.println(ConnectTL("302a5158e576eed0adc7b98010a5128a").ping());
//        try {
//            TestLinkAPI api = TestlinkApiUtil.ConnectTL("40203cf32048045a42602f71c16102b9");
//            //int id= TestlinkApiUtil.getProjectIdByProjectName(api,"中测试");
//            //TestSuite[] testSuites = getTestsuit(api,id);
//            //TestPlan testPlan = api.getTestPlanByName("2","中测试");
////            List<Object[]> mapList = getTestCaseList(api,testPlan.getId(),getBuildId(api,testPlan.getId(),"sd4"),id);
//            List<Object[]> mapList = getTestCaseList(api,4465,7,1631);
//            Object[] o = mapList.get(0);
//            System.out.println(mapList.toString());
//            System.out.println((String)o[2]);
//
//        } catch (FrameworkException e) {
//            e.printStackTrace();
//        }

        try {
            deleteTestcaseStep();
        } catch (FrameworkException e) {
            e.printStackTrace();
        }
    }
}
