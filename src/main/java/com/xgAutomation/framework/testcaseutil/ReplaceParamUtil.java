package com.xgAutomation.framework.testlink;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.utils.StringUtils;
import org.apache.commons.lang.RandomStringUtils;

import redis.clients.jedis.params.Params;

/**
 * Created by MingWei on 2018/6/27
 * Description
 */
public class ReplaceParamUtil {

    private  ReplaceParamUtil(){}

    public static String paramHandle(String resource) throws FrameworkException {

        String param = resource.substring(resource.indexOf("{") + 1,resource.length() - 1);
        String solvedString = "";
        switch (param) {
            case "email" :
                solvedString = StringUtils.getRandomEmail();
                break;
            default :
                throw new FrameworkException("json里的参数没有定义");
        }
        return solvedString;
    }
}
