package com.xgAutomation.framework.testlink;

/**
 * Created by liuqq on 2018/6/7.
 */
public enum CaseParams {

    SETPS("steps"),//步骤
    CASEID("caseId"),//用例id
    CUSTOM_FIELD("custom_field"),//用来存自定义字段
    SUMMARY("summary"),//用例摘要用来存class_method
    ExpectedResult("expectedResult");//预期结果
    private String value;

    CaseParams(String value) {
        this.value = value;
    }

    /**
     * Print param value.
     * @return param value
     */
    public String toString() {
        return this.value;
    }

}
