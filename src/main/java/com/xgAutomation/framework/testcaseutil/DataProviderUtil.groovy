package com.xgAutomation.framework.testlink;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import com.xgAutomation.framework.exception.FrameworkException
import com.xgAutomation.testcase.Sinotrans.waiter.system.test;
import groovy.json.JsonSlurper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Properties;

import static com.xgAutomation.framework.testlink.TestlinkApiUtil.getBuildId;

/**
 * Created by MingWei on 2018/6/11
 * Description Testlink数据驱动  返回用例的参数数组，一维为索引，二维为用例属性数组
 */
public class DataProviderUtil {

    private DataProviderUtil() {
    }

    public static Logger logger = LoggerFactory.getLogger("DataProviderUtil");

    public static Object[][] getSinotransDataProvider(TestLinkAPI api, Properties configProp, int testPlanId) throws FrameworkException {

        Object[][] ob = null;
        int projectId = 0;
        int buildId = 0;
        List<Object[]> list = null;

        projectId = TestlinkApiUtil.getProjectIdByProjectName(api, configProp.getProperty("ProjectName"));
        buildId = getBuildId(api, testPlanId, configProp.getProperty("BuildName"));
        if (projectId != 0 && buildId != 0) {
            list = TestlinkApiUtil.getTestCaseList(api, testPlanId, buildId, projectId);
        } else {
            throw new FrameworkException("projectId或buildId 等于0 ");
        }

        if (list != null && list.size() > 0) {
            ob = new Object[list.size()][];
            for (int i = 0; i < list.size(); i++) {
                ob[i] = list.get(i);
            }

        } else {
            throw new FrameworkException("获取到的测试用例为空");
        }
        logger.debug("ob" + ob.toString());
        return ob
    }

    public static void getDataProvider(String filePath) throws FrameworkException {
        JsonSlurper slurper = new JsonSlurper()
        def jsonPayload = new File(filePath).text
        def jsonDP = slurper.parseText(jsonPayload)
        println jsonDP.
    }

     static void main(args) {
         getDataProvider("src/main/resources/testcase/test.json")
    }


}
