package com.xgAutomation.testcase.Kuafu;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import com.xgAutomation.framework.exception.ExceptionUtil;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.meta.ServerEnum;
import com.xgAutomation.framework.testlink.DataProviderUtil;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.ReflectUtils;
import com.xgAutomation.framework.utils.StringUtils;
import org.slf4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

/**
 * Created by MingWei on 2018/12/4
 * Description
 */
public class KuafuTestEngine {

        Logger logger;
        private int testPlanId,buildId;
        private TestLinkAPI api;
        private Properties configProp;

        @BeforeTest
        public void setUp() {
            ReflectUtils.init(configProp,api,testPlanId,buildId,logger);
            LogService.debug(this.getClass(), LogType.Init, "-----初始化结果-----" +
                    "\r\napi = " + api.getUrl().toString() +
                    "\r\ntestPlanId : " + testPlanId +
                    "\r\nbuildId = " + buildId);
        }

        @Test(dataProvider = "dataPro", testName = "sinotransTest")
        public void sinotransTest(String url, String method,String testcaseDesprition, String expectedResult) throws FrameworkException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
            logger.info("----------开始执行" + " " + testcaseDesprition + "用例" + "----------");
            LogService.debug(this.getClass(), LogType.Init, "-----数据驱动获取的参数为-----" +
                    "\r\nurl = " + url +
                    "\r\nmethod = " + method +
                    "\r\ntestcaseDesprition = " + testcaseDesprition +
                    "\r\nexpectedResult = " + expectedResult);

            //url = ReflectUtils.urlHandle(url, ServerEnum.SINOTRANS);


        }



        @DataProvider(parallel = true)
        public Object[][] dataPro() {
            Object[][] ob = null;
            logger.info("----------开始执行测试数据驱动模块----------");
            try {
                ob = DataProviderUtil.getDataProvider(api,);
            } catch (FrameworkException e) {
                LogService.error(DataProviderUtil.class, LogType.DataProvider, e.getMessage());
                e.printStackTrace();
            }

            logger.info("----------执行测试数据驱动模块结束----------");
            return ob;
        }

    }

}
