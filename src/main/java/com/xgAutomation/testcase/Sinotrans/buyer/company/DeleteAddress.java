package com.xgAutomation.testcase.Sinotrans.buyer.company;

import com.xgAutomation.dataPrepare.sinotrans.DataUtil;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class DeleteAddress extends com.xgAutomation.testcase.Sinotrans.InterfaceTestcase {

    public Boolean deleteAddressTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        Long id = DataUtil.getAddressId(client, re, jsonObject.getString("accessToken"));
        jsonObject.put("id", id);
        response = re.okHttpPost_Form(client, url, jsonObject.toString());
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
