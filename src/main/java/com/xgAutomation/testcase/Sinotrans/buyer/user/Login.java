package com.xgAutomation.testcase.Sinotrans.buyer.user;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.Encrypt.Base64Util;
import com.xgAutomation.framework.utils.JsonUtil.JsonParse;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class Login extends InterfaceTestcase {

    public Boolean loginTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        JSONObject checkjson = JSONObject.fromObject(jsonBody);
        String password = checkjson.getString("password");
        String base64Password = Base64Util.encode(password.getBytes());
        String solvedJson = JsonParse.replaceValueByJpathString(jsonBody, "password", base64Password);
        response = re.okHttpPost_Form(client, url, solvedJson);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
