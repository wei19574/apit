package com.xgAutomation.testcase.Sinotrans.buyer.company;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class GetCompanyInfo extends com.xgAutomation.testcase.Sinotrans.InterfaceTestcase {

    public Boolean getCompanyInfoTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        response = re.okhttp_get(client, url, "");
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
