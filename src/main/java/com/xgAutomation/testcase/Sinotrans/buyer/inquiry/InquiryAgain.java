package com.xgAutomation.testcase.Sinotrans.buyer.inquiry;

import com.xgAutomation.dataPrepare.sinotrans.DataUtil_seller;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class InquiryAgain extends InterfaceTestcase {

    public Boolean inquiryAgainTest(String url, String json, String expectedBody) throws FrameworkException, IOException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        Map resultMp = DataUtil_seller.getTenderOrderNo(client, re);
        Map replaceMp = new HashMap();
        replaceMp.put("tenderOrderNo", resultMp.get("tenderOrderNo"));
        jsonBody = ConfigUtils.checkJson(client, re, json);
        String solvedJsonbody = ConfigUtils.replaceJson(replaceMp, jsonBody);
        response = re.okHttpPost_Form(client, url, solvedJsonbody);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
