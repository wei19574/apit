package com.xgAutomation.testcase.Sinotrans.buyer.user;

import com.xgAutomation.dataPrepare.sinotrans.DBFactory;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.DBUtil.MysqlClient;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class CreateUser extends InterfaceTestcase {

    public Boolean createUserTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        MysqlClient mc =DBFactory.getMysqlClient(DBFactory.USER_DATABASE);
        Boolean result = false;
        Response response;
        String jsonBody;
        jsonBody = ConfigUtils.checkJson(client, re, json);
        response = re.okHttpPost_Form(client, url, jsonBody);
        mc.ExecuteSingleUpdateSQL("delete from tb_user where email = 'wm195723931@163.com';");
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;
        mc.close();
        return result;
    }

}
