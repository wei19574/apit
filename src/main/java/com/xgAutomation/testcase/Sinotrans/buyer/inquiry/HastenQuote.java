package com.xgAutomation.testcase.Sinotrans.buyer.inquiry;

import com.xgAutomation.dataPrepare.sinotrans.DataUtil_seller;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class HastenQuote extends InterfaceTestcase {

    public Boolean hastenQuoteTest(String url, String json, String expectedBody) throws FrameworkException, IOException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        String inquiryOrderNo = DataUtil_seller.getInquiryOrderNo(client, re);
        jsonBody = ConfigUtils.checkJson(client, re, json);
        Map<String, Object> map = new HashMap<>();
        map.put("inquiryOrderNo", inquiryOrderNo);
        String jsonbody = ConfigUtils.replaceJson(map, jsonBody);
        response = re.okHttpPost_Form(client, url, jsonbody);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}