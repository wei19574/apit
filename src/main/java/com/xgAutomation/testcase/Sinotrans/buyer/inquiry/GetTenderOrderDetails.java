package com.xgAutomation.testcase.Sinotrans.buyer.inquiry;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class GetTenderOrderDetails extends InterfaceTestcase {

    public Boolean getTenderOrderDetailsTest(String url, String json, String expectedBody) throws FrameworkException, IOException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        jsonBody = ConfigUtils.checkJson(client, re, json);
        response = re.okHttpPost_Form(client, url, jsonBody);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
