package com.xgAutomation.testcase.Sinotrans.buyer.sample;

import com.xgAutomation.dataPrepare.sinotrans.DBFactory;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.DBUtil.MysqlClient;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class PaySamplePrice extends InterfaceTestcase {

    public Boolean paySamplePriceTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        MysqlClient mc = DBFactory.getMysqlClient(DBFactory.INQ_DATABASE);

        mc.ExecuteSingleUpdateSQL("update tb_sample_order set status = '2' where sample_order_no = 2020180704171716811451;");
        jsonBody = ConfigUtils.checkJson(client, re, json);
        response = re.okHttpPost_Form(client, url, jsonBody);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;
        int res = mc.ExecuteSingleUpdateSQL("update tb_sample_order set status = '2' where sample_order_no = 2020180704171716811451;");
        if (res == 0) {
            mc.close();
            throw new FrameworkException("sql执行失败");
        }

        return result;
    }

}
