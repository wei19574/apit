package com.xgAutomation.testcase.Sinotrans.waiter.item;


import com.xgAutomation.dataPrepare.sinotrans.DataUtil;
import com.xgAutomation.dataPrepare.sinotrans.DataUtil_seller;
import com.xgAutomation.dataPrepare.sinotrans.Login;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class DownItem extends InterfaceTestcase {

    public Boolean downItemTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        String sellerToken = Login.login_sellerDefault(client,re);
        Map map = DataUtil_seller.createItem(client,re,sellerToken);
        map.put("itemNos", map.get("itemNo"));
        jsonBody = ConfigUtils.checkJson(client, re, json);
        String jsonLast = ConfigUtils.replaceJson(map, jsonBody);
        response = re.okHttpPost_Form(client, url, jsonLast);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
