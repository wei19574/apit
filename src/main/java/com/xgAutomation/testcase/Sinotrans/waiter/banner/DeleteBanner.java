package com.xgAutomation.testcase.Sinotrans.waiter.banner;


import com.xgAutomation.dataPrepare.sinotrans.DataUtil;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class DeleteBanner extends InterfaceTestcase {

    public Boolean DeleteBannerTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        String bannerId = DataUtil.getBannerId(client, re, jsonObject.getString("accessToken"));
        Map<String, Object> map = new HashMap<>();
        map.put("id", bannerId);
        String jsonLast = ConfigUtils.replaceJson(map, jsonBody);
        response = re.okHttpPost_Form(client, url, jsonLast);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
