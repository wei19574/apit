package com.xgAutomation.testcase.Sinotrans.waiter.system.user;


import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.StringUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;

/**
 * Created by liuqq on 2018/6/12.
 */
public class AddUser extends InterfaceTestcase {

    public Boolean addUserTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        //替换json里面的username字段
        String userName = StringUtils.getRandomString(12);
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        jsonObject.put("username", userName);
        response = re.okHttpPost_Form(client, url, jsonObject.toString());
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;
        LogService.info(AddUser.class, LogType.BIZ, "执行结束——" + "addUserTest");

        return result;
    }

}

