package com.xgAutomation.testcase.Sinotrans.seller.shop;


import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.StringUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class UpdateShopAccountInfo extends InterfaceTestcase {

    public Boolean updateShopAccountInfoTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        String accountName = StringUtils.getRandomString(12);
        Map<String, Object> map = new HashMap<>();
        map.put("accountName", accountName);
        String jsonLast = ConfigUtils.replaceJson(map, jsonBody);
        response = re.okHttpPost_Form(client, url, jsonLast);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
