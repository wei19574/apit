package com.xgAutomation.testcase.Sinotrans.seller.item;


import com.xgAutomation.dataPrepare.sinotrans.DataUtil_seller;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class BatchOffShelfItem extends InterfaceTestcase {

    public Boolean batchOffShelfItemTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        jsonBody = ConfigUtils.checkJson(client, re, json);
        JSONObject jsonObject = JSONObject.fromObject(jsonBody);
        String itemNo = DataUtil_seller.getItemNoByCheck(client, re, jsonObject.getString("accessToken"));
        jsonObject.put("itemNos", itemNo);
        response = re.okHttpPost_Form(client, url, jsonObject.toString());
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
