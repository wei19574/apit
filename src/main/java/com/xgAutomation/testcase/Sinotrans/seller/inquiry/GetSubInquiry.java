package com.xgAutomation.testcase.Sinotrans.seller.inquiry;


import com.xgAutomation.dataPrepare.sinotrans.DataUtil_seller;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class GetSubInquiry extends InterfaceTestcase {

    public Boolean getSubInquiryTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        JSONObject sjson = JSONObject.fromObject(jsonBody);
        String acessToekn = sjson.getString("accessToken");
        String inquiryOrderNo = DataUtil_seller.getInquiryOrderNo(client, re);
        Map<String, Object> map = new HashMap<>();
        map.put("inquirySubOrderNo", DataUtil_seller.getInquirySubOrderNo(client, re, inquiryOrderNo, acessToekn));
        String jsonLast = ConfigUtils.replaceJson(map, jsonBody);
        response = re.okHttpPost_Form(client, url, jsonLast);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
