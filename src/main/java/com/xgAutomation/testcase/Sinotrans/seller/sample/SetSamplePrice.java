package com.xgAutomation.testcase.Sinotrans.seller.sample;


import com.xgAutomation.dataPrepare.sinotrans.DataUtil_seller;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.Map;


public class SetSamplePrice extends InterfaceTestcase {

    public Boolean setSamplePriceTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        String sellerAccessToken = com.xgAutomation.dataPrepare.sinotrans.Login.login_sellerDefault(client, re);
        Map<String, Object> map = DataUtil_seller.getSampleOrderNo(client, re, sellerAccessToken);
        jsonBody = ConfigUtils.checkJson(client, re, json);
        String jsonLast = ConfigUtils.replaceJson(map, jsonBody);
        response = re.okHttpPost_Form(client, url, jsonLast);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
