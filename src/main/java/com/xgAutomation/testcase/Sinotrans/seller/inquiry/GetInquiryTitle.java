package com.xgAutomation.testcase.Sinotrans.seller.inquiry;


import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class GetInquiryTitle extends InterfaceTestcase {

    public Boolean getInquiryTitleTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;
        jsonBody = ConfigUtils.checkJson(client, re, json);
//            Map<String,Object> map = new HashMap<>();
//            map.put("inquiryOrderNo",DataUtil_seller.getInquiryOrderNo(client,re));
//            String jsonLast = ConfigUtils.replaceJson(map,jsonBody);
        response = re.okHttpPost_Form(client, url, jsonBody);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
