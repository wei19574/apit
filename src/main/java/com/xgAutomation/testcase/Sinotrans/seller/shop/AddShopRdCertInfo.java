package com.xgAutomation.testcase.Sinotrans.seller.shop;


import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.JsonUtil.JsonParse;
import com.xgAutomation.framework.utils.StringUtils;
import com.xgAutomation.testcase.Sinotrans.InterfaceTestcase;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;


public class AddShopRdCertInfo extends InterfaceTestcase {

    public Boolean addShopRdCertInfoTest(String url, String json, String expectedBody) throws IOException, FrameworkException {
        RequestFactory re = super.getRe();
        OkHttpClient client = super.getClient();
        Boolean result = false;
        Response response;
        String jsonBody;

        jsonBody = ConfigUtils.checkJson(client, re, json);
        String certName = StringUtils.getRandomString(12);
        String jsonLast = JsonParse.replaceValueByJpath(JSONObject.fromObject(jsonBody), "rdInfoList[0]/certName", certName);
        response = re.okHttp_Post_Testlink(client, url, jsonLast);
        AssertUtil.checkTestlinkResult(response.getBody(), expectedBody);
        result = true;

        return result;
    }

}
