package com.xgAutomation.testcase.Sinotrans;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import com.xgAutomation.framework.exception.ExceptionUtil;
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.testlink.DataProviderUtil;
import com.xgAutomation.framework.testlink.TestlinkApiUtil;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import com.xgAutomation.framework.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import static com.xgAutomation.framework.testlink.TestlinkApiUtil.getBuildId;

/**
 * Created by MingWei on 2018/6/7
 * Description TC执行引擎
 */
public class sinotransTestEngine {

    Logger logger = LoggerFactory.getLogger("sinotransTestEngine");
    private int testPlanId;
    private int buildId;
    private TestLinkAPI api;
    private Properties configProp;
    private Properties urlProp;

    @BeforeTest
    public void setUp() {
        logger.info("----------开始执行初始化----------");
        configProp = PropertiesFileReader.readProperties(ConfigUtils.config);
        urlProp = PropertiesFileReader.readProperties(ConfigUtils.defaultURL);
        try {
            api = TestlinkApiUtil.ConnectTL(configProp.getProperty("DevKey"));
            testPlanId = api.getTestPlanByName(configProp.getProperty("PlanName"), configProp.getProperty("ProjectName")).getId();
            buildId = getBuildId(api, testPlanId, configProp.getProperty("BuildName"));
            LogService.debug(this.getClass(), LogType.Init, "-----初始化结果-----" +
                    "\r\napi = " + api.getUrl().toString() +
                    "\r\ntestPlanId : " + testPlanId +
                    "\r\nbuildId = " + buildId);
        } catch (FrameworkException f) {
            LogService.error(TestlinkApiUtil.class, LogType.REQUEST, f.getMessage());
            f.printStackTrace();
        }
        logger.info("----------初始化成功----------");
    }

    @Test(dataProvider = "dataPro", testName = "sinotransTest")
    public void sinotransTest(String summary, String url, String step, int testcaseId, String Preconditions, String expectedResult) throws FrameworkException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        logger.info("----------开始执行" + " " + StringUtils.removeBackspace(summary) + "用例" + "----------");
        LogService.debug(this.getClass(), LogType.Init, "-----数据驱动获取的参数为-----" +
                "\r\ntestcaseId = " + testcaseId +
                "\r\nstep = " + step +
                "\r\nsummary = " + summary +
                "\r\nPreconditions = " + Preconditions +
                "\r\nexpectedResult = " + expectedResult);
        //String url = custom_field.get(CustomFieldsParams.URL.toString());
        //String subUrl = url.substring(1,url.indexOf("/",1));
        switch (url.substring(1, url.indexOf("/", 1))) {
            case "sino-buyer-web":
                url = urlProp.getProperty("sino-buyer-web") + url;
                break;
            case "sino-seller-web":
                url = urlProp.getProperty("sino-seller-web") + url;
                break;
            case "sino-manager-web":
                url = urlProp.getProperty("sino-manager-web") + url;
                break;
        }
        String methodName = "";
        String className = "";
        Boolean executeResult = false;
        String exceptionInfo = "";
        //处理传入的前提  com.xgAutomation.testcase.Interface.Sinotrans.sino.system.user.userTest#adduserTest
        if (Preconditions != null && Preconditions.contains("#")) {
            String[] preconditions = Preconditions.split("#");
            className = preconditions[0];
            methodName = preconditions[1];
        } else {
            throw new IllegalArgumentException("前提填写不符合规范");
        }
        Class clz = null;
        //通过反射调用传入的class&method
        try {
            clz = Class.forName(className);
            Object reflectOb = clz.newInstance();

            Method method = clz.getMethod(methodName, String.class, String.class, String.class);

            Object invokeOb = method.invoke(reflectOb, url, step, expectedResult);

            executeResult = (Boolean) invokeOb;
        } catch (Exception i) {
            exceptionInfo = ExceptionUtil.getErrorInfoFromException(i);
        }
        //判断用例执行结果，并回传到testlink
        if (executeResult != null) {
            if (executeResult == true) {
                api.reportTCResult(testcaseId, null, testPlanId, ExecutionStatus.PASSED, buildId, configProp.getProperty("BuildName"), null, null, null, null, null, null, true);
                logger.info("----------执行" + StringUtils.removeBackspace(summary) + "用例结果为" + "通过" + "----------");
                Assert.assertTrue(true);
            } else {
                api.reportTCResult(testcaseId, null, testPlanId, ExecutionStatus.FAILED, buildId, configProp.getProperty("BuildName"), null, null, null, null, null, null, true);
                logger.info("----------执行" + StringUtils.removeBackspace(summary) + "用例结果为" + "不通过" + "----------");
                LogService.error(clz.getClass(), LogType.DataProvider, exceptionInfo);
                throw new AssertionError(exceptionInfo);
            }
        }
    }

    @DataProvider(parallel = true)
    public Object[][] dataPro() {
        Object[][] ob = null;
        logger.info("----------开始执行测试数据驱动模块----------");
        try {
            ob = DataProviderUtil.getDataProvider(api, configProp, testPlanId);
        } catch (FrameworkException e) {
            LogService.error(DataProviderUtil.class, LogType.DataProvider, e.getMessage());
            e.printStackTrace();
        }

        logger.info("----------执行测试数据驱动模块结束----------");
        return ob;
    }

}
