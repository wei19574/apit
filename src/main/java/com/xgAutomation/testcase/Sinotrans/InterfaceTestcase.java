package com.xgAutomation.testcase.Sinotrans;

import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MingWei on 2018/7/2
 * Description
 */
public class InterfaceTestcase {

    public final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();
    RequestFactory re = new RequestFactory();
    Response response;
    OkHttpClient client = new OkHttpClient.Builder()
            .cookieJar(new CookieJar() {
                @Override
                public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                    cookieStore.put(httpUrl.host(), list);
                }

                @Override
                public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                    List<Cookie> cookies = cookieStore.get(httpUrl.host());
                    return cookies != null ? cookies : new ArrayList<Cookie>();
                }
            })
            .build();

    public HashMap<String, List<Cookie>> getCookieStore() {
        return cookieStore;
    }

    public OkHttpClient getClient() {
        return client;
    }

    public void setClient(OkHttpClient client) {
        this.client = client;
    }

    public RequestFactory getRe() {
        return re;
    }

    public void setRe(RequestFactory re) {
        this.re = re;
    }
}
