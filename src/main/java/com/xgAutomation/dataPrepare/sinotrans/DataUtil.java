package com.xgAutomation.dataPrepare.sinotrans;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.JsonReader.JsonFileReaderUtil;
import com.xgAutomation.framework.utils.JsonUtil.JsonParse;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import com.xgAutomation.framework.utils.StringUtils;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;
import org.intellij.lang.annotations.Language;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by liuqq on 2018/6/13.
 */
public class DataUtil{


    /**
     * 创建买家用户，随机的用户名
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> getUserName(OkHttpClient client, RequestFactory re)throws FrameworkException,IOException{
        //source-来源：web/app,areaCode-联系方式区号，新增用户
        String jsonDefaultCreate = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultCreateUser.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultCreate);
        //生成随机的字符作为用户名
        String userName = StringUtils.getRandomString(6);
        String user = userName+"@163.com";
        jsonObject.put("email",user);
        Response response = null;
        Map<String,Object> map = new HashMap<>();
//        try{
            //创建用户
            //response = re.okHttpPost_Form(client,Url.urlCreateUser,jsonObject.toString());
            //查询id
//            response = re.okHttpPost_Form(client,urlGetUser,JSONObject.fromObject(userMap).toString());
//            map.put("id",JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/id"));
//        }catch (FrameworkException e){
//            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
//        }
        //获取用户id
        response = re.okHttpPost_Form(client,Url.urlCreateUser,jsonObject.toString());
        if(JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/tempAccessToken") != null) {
            map.put("tempAccessToken", JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()), "data/tempAccessToken"));
            map.put("email",user);
        }else{
            throw new FrameworkException("调用创建用户方法失败");
        }
        return map;
    }

    /**
     * 获取审核通过的用户
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> getUserByChesk(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        //审核用户
        String jsonDefaultCheck = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultCheck.json");
        Map<String,Object> user = getUserName(client,re);
        user.put("accessToken",accessToken);
        String checkUser = ConfigUtils.replaceJson(user,jsonDefaultCheck);
        try{
            re.okHttpPost_Form(client,Url.urlCheckUser,checkUser);
        }catch (FrameworkException e){
            LogService.error(DataUtil.class,LogType.DataProvider,e.getMessage());
            return null;
        }
        return user;
    }


    /**
     * 创建店铺，随机的名
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> createShop(OkHttpClient client, RequestFactory re)throws FrameworkException,IOException{
        //创建店铺
        String jsonDefaultShop = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultShop.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultShop);
        //生成随机的字符作为店铺
        String shopName = StringUtils.getRandomString(12);
        jsonObject.put("shopName",shopName);
        //登录卖家端
        String accessToken = "";
        Map<String,Object> map = new HashMap<>();
        try{
            accessToken = Login.login_sellerDefault(client,re);
            jsonObject.put("accessToken",accessToken);
            Response response = re.okHttpPost_Form(client,Url.addShopUrl,jsonObject.toString());
            Long id = Long.valueOf(JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/shopId"));
            String shopNo = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/shopNo");
            map.put("id",id);
            map.put("shopNo",shopNo);

        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }

        map.put("shopName",shopName);
        return map;
    }

    /**
     * 获取店铺编号
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static String getShopNo(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        //获取店铺列表
        String jsonDefaultShopPage = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultShopPage.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultShopPage);
        jsonObject.put("accessToken",accessToken);
        Map<String,Object> map = createShop(client,re);
        String json = ConfigUtils.replaceJson(map,jsonObject.toString());
        String dataString = null;
        try{
            Response response = re.okHttpPost_Form(client,Url.urlDefaultShopList,json);
            dataString = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/shopNo");
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        return dataString;
    }

    /**
     * 获取已经审核过的店铺编号
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static String getShopNoByCheck(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        //审核店铺
        String jsonDefaultCheckShop = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultCheckShop.json");
        String shopNo = getShopNo(client,re,accessToken);
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultCheckShop);
        jsonObject.put("shopNo",shopNo);
        jsonObject.put("accessToken",accessToken);
        try{
            re.okHttpPost_Form(client,Url.urlDefaultCheckShop,jsonObject.toString());
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        return shopNo;
    }

    /**
     * 创建广告，获取广告id
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static String getAdsId(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
//新增广告
        String jsonDefaultAddAds = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultAddAds.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultAddAds);
        jsonObject.put("accessToken",accessToken);
        Response response;
        String adsId = null;
        try{
           response =  re.okHttpPost_Form(client,Url.urlAddAds,jsonObject.toString());
            adsId = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/id");
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        if (adsId == null){
            throw new FrameworkException("获取广告id失败");
        }
        return adsId;
    }

    /**
     * 创建banner，获取广告id
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static String getBannerId(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        //新增banner
        String jsonDefaultAddBanner = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultAddBanner.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultAddBanner);
        jsonObject.put("accessToken",accessToken);
        Response response;
        String bannerId = null;
        try{
            response =  re.okHttpPost_Form(client,Url.urlAddBanner,jsonObject.toString());
            bannerId = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/id");
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        if (bannerId == null){
            throw new FrameworkException("获取bannerid失败");
        }
        return bannerId;
    }

    /**
     * 创建floor，获取橱窗
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static Map<String,Object> getFloor(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        String name = StringUtils.getRandomString(12);
        //新增橱窗
        String jsonDefaultAddFloor = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultAddFloor.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultAddFloor);
        jsonObject.put("accessToken",accessToken);
        jsonObject.put("name",name);
        Response response;
        String floorId = null;
        Long pageId = jsonObject.getLong("pageId");
        try{
            response = re.okHttp_Post_Testlink(client,Url.urlAddFloor,jsonObject.toString());
            floorId = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/id");
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        Map<String,Object> map = new HashMap<>();
        map.put("id",floorId);
        map.put("name",name);
        map.put("pageId",pageId);
        return map;
    }


    /**
     * 获取类目no
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static String getCategoryNo(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        //小二端后台添加类目
        String jsonDefaultGetCateNo = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultGetCateNo.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultGetCateNo);
        jsonObject.put("accessToken",accessToken);
        String name = StringUtils.getRandomString(12);
        jsonObject.put("name",name);
        String dataString = null;
        try{
            Response response = re.okHttpPost_Form(client,Url.urlAddBackCategory,jsonObject.toString());
            dataString = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/cateNo");
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        return dataString;
    }


    /**
     * 获取类目no
     * @param client
     * @param re
     * @param accessToken
     * @return
     */
    public static Long getAddressId(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        String jsonDefaultGetCateNo = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultAddAddress.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultGetCateNo);
        jsonObject.put("accessToken",accessToken);
        Long id = null;
        try{
            Response response = re.okHttpPost_Form(client,Url.addAddress,jsonObject.toString());
            id =Long.valueOf(JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data/id"));
        }catch (FrameworkException e){
            LogService.error(DataUtil.class, LogType.DataProvider,e.getMessage());
            return null;
        }
        return id;
    }


}
