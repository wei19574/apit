package com.xgAutomation.dataPrepare.sinotrans;

import com.mongodb.DB;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.DBUtil.MysqlClient;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by MingWei on 2018/7/5
 * Description
 */
public class DBFactory {

    public static Properties configProp = PropertiesFileReader.readProperties(ConfigUtils.config);


    public final static String CATE_DATABASE ="db_sino_cate_test";
    public final static String ITEM_DATABASE ="db_sino_item_test";
    public final static String SHOP_DATABASE ="db_sino_shop_test";
    public final static String USER_DATABASE ="db_sino_user_test";
    public final static String MISC_DATABASE ="db_sino_misc_test";
    public final static String INQ_DATABASE ="db_sino_inq_test";


    public static MysqlClient getMysqlClient(String instanceName){
        String host = configProp.getProperty("host");
        String port = configProp.getProperty("port");
        String userName = configProp.getProperty("username");
        String passWord = configProp.getProperty("password");
        return new MysqlClient(host,port,instanceName,userName,passWord);
    }

    public MysqlClient getMysqlClient(String host,String port,String instanceName,String username,String password){
        return new MysqlClient(host,port,instanceName,username,password);
    }

    public static void main(String[] args) throws SQLException {
        MysqlClient mc = DBFactory.getMysqlClient(INQ_DATABASE);
        int res = mc.ExecuteSingleUpdateSQL("update tb_sample_order set status = '6' where sample_order_no = '2020180705103230306948';");
        System.out.println(res);
        ResultSet rs = mc.ExecuteSelectSQL("select sample_order_no from tb_sample_order where status = '6';");

        while(rs.next()){
            String sample_order_no = rs.getString(1);
            System.out.println(sample_order_no);
        }

        mc.close();
    }

}
