package com.xgAutomation.dataPrepare.sinotrans;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.JsonReader.JsonFileReaderUtil;
import com.xgAutomation.framework.utils.JsonUtil.JsonParse;
import com.xgAutomation.framework.utils.LogReader.LogService;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;

/**
 * Created by MingWei on 2018/6/29
 * Description
 */
public class Token {


    public static String getToken(String email, String password, String role) throws FrameworkException, IOException {

        String accessToken = "";
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        RequestFactory re = new RequestFactory();


            if(role.equals(RoleType.SELLER)){
                String sellerDefault = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/sellerDefault.json");
                JSONObject jsonObject = JSONObject.fromObject(sellerDefault);
                jsonObject.put("email",email);
                jsonObject.put("password",password);
                Response response = re.okHttpPost_Form(client,Url.loginSeller,jsonObject.toString());
                if (response!=null&&response.getCode() == 1) {
                    JSONObject jsonResponse = JSONObject.fromObject(response.getBody());
                    accessToken = JsonParse.getValueByJpath(jsonResponse,"data/accessToken");
                    //切换卖家买家身份
                    DataUtil_seller.choverldentity(client,re,accessToken);
                }else {
                    throw new FrameworkException("登录失败");
                }
        LogService.debug(Token.class, LogType.REQUEST,"获得的token为 ： " + accessToken);

    }
        return accessToken;
    }

    public static void main(String[] args) {
//        String s = "java";
//        StringBuilder sb = new StringBuilder(s);
//        System.out.println(sb.reverse());
//
//        char[] c = s.toCharArray();
//        for(int i=c.length-1;i >= 0;i--){
//            System.out.println(c[i]);
//        }
//
//        for(int i=c.length-1;i >= 0;i--){
//            System.out.println(s.charAt(i));
//        }
//
//        s = 'python';
//        print s[::-1]
//
//        l = list[s];
//        l.reverse();
//        print ''join(l)

    }
}