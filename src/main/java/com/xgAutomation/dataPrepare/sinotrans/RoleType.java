package com.xgAutomation.dataPrepare.sinotrans;

/**
 * Created by MingWei on 2018/6/29
 * Description
 */
public class RoleType {

    public static final String SELLER = "seller";
    public static final String BUYER ="buyer";
    public static final String WAITER ="waiter";
}
