package com.xgAutomation.dataPrepare.sinotrans;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Encrypt.Base64Util;
import com.xgAutomation.framework.utils.JsonReader.JsonFileReaderUtil;
import com.xgAutomation.framework.utils.JsonUtil.JsonParse;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;
import com.xgAutomation.framework.utils.ConfigUtils;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by MingWei on 2018/6/8
 * Description
 */
public class Login {



    //小二端登录
    public static String login_waiter(OkHttpClient client,RequestFactory re,String json) throws FrameworkException {
        Response response = re.okHttpPost_Form(client,Url.loginSino,json);
        String accessToken = null;
        if (response!=null&&response.getCode() == 1) {
            JSONObject jsonResponse = JSONObject.fromObject(response.getBody());
            accessToken = JsonParse.getValueByJpath(jsonResponse,"data/accessToken");
        }else {
            throw new FrameworkException("登录失败");
        }
        return accessToken;
    }

//    //默认卖家登录
    public static String login_sellerDefault(OkHttpClient client,RequestFactory re) throws FrameworkException,IOException {
        String sellerDefault = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/sellerDefault.json");
        byte[] sellerbyte = JSONObject.fromObject(sellerDefault).getString("password").getBytes();
        String sellerJson = JsonParse.replaceValueByJpathString(sellerDefault,"password",Base64Util.encode(sellerbyte));
        Response response = re.okHttpPost_Form(client,Url.loginSeller,sellerJson);
        String accessToken = null;
        if (response!=null&&response.getCode() == 1) {
            JSONObject jsonResponse = JSONObject.fromObject(response.getBody());
            accessToken = JsonParse.getValueByJpath(jsonResponse,"data/accessToken");
        }else {
            throw new FrameworkException("登录失败");
        }
        return accessToken;
    }

    //买家登录
    public static String login(OkHttpClient client, RequestFactory re, Map<String,String> mp) throws FrameworkException ,IOException{
        //String json = ConfigUtils.replaceJson(map,sellerDefault);
        String sellerDefault = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/sellerDefault.json");
        JSONObject jsonObject = JSONObject.fromObject(sellerDefault);
        byte[] passBy = mp.get("password").getBytes();
        jsonObject.put("email",mp.get("email"));
        jsonObject.put("password",Base64Util.encode(passBy));
        Response response = re.okHttpPost_Form(client,Url.loginSeller,jsonObject.toString());
        String accessToken = null;
        if (response!=null&&response.getCode() == 1) {
            JSONObject jsonResponse = JSONObject.fromObject(response.getBody());
            accessToken = JsonParse.getValueByJpath(jsonResponse,"data/accessToken");
            if (accessToken == null){
                accessToken = JsonParse.getValueByJpath(jsonResponse,"data/tempAccessToken");
            }
        }else {
            throw new FrameworkException("登录失败");
        }

        return accessToken;
    }

    //卖家登录
    public static String login_seller(OkHttpClient client, RequestFactory re, Map<String,String> mp) throws FrameworkException ,IOException{
        //String json = ConfigUtils.replaceJson(map,sellerDefault);
        String sellerDefault = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/sellerDefault.json");
        JSONObject jsonObject = JSONObject.fromObject(sellerDefault);
        byte[] passBy = mp.get("password").getBytes();
        jsonObject.put("email",mp.get("email"));
        jsonObject.put("password",Base64Util.encode(passBy));
        Response response = re.okHttpPost_Form(client,Url.loginSeller,jsonObject.toString());
        String accessToken = null;
        if (response!=null&&response.getCode() == 1) {
            JSONObject jsonResponse = JSONObject.fromObject(response.getBody());
            accessToken = JsonParse.getValueByJpath(jsonResponse,"data/accessToken");
        }else {
            throw new FrameworkException("登录失败");
        }

        return accessToken;
    }

    //默认买家登录
    public static String login_buyerDefault(OkHttpClient client,RequestFactory re) throws FrameworkException,IOException {
        String buyerDefault = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/buyerDefault.json");
        byte[] buyerbyte = JSONObject.fromObject(buyerDefault).getString("password").getBytes();
        String buyerJson = JsonParse.replaceValueByJpathString(buyerDefault,"password",Base64Util.encode(buyerbyte));
        Response response = re.okHttpPost_Form(client,Url.loginSeller,buyerJson);
        String accessToken = null;
        if (response!=null&&response.getCode() == 1) {
            JSONObject jsonResponse = JSONObject.fromObject(response.getBody());
            accessToken = JsonParse.getValueByJpath(jsonResponse,"data/accessToken");
        }else {
            throw new FrameworkException("登录失败");
        }
        return accessToken;
    }

}
