package com.xgAutomation.dataPrepare.sinotrans;

import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.PropertyReader.PropertiesFileReader;

import java.util.Properties;

/**
 * Created by MingWei on 2018/7/4
 * Description
 */
public class Url {
    public static Properties prop = PropertiesFileReader.readProperties(ConfigUtils.defaultURL);

    //manage
    public final static String urlDefaultShopList=  prop.getProperty("sino-manager-web") +  "/sino-manager-web/manager/member/shop/query_shop_page";
    public final static String urlDefaultCheckShop= prop.getProperty("sino-manager-web") + "/sino-manager-web/manager/member/shop/check_shop";
    public final static String urlAddAds=     prop.getProperty("sino-manager-web") +           "/sino-manager-web/manager/marketing/ads/add_ads";
    public final static String urlAddBanner = prop.getProperty("sino-manager-web") +       "/sino-manager-web/manager/marketing/banner/add_banner";
    public final static String urlAddFloor =  prop.getProperty("sino-manager-web") +        "/sino-manager-web/manager/marketing/floor/add_floor";
    public final static String urlAddBackCategory = prop.getProperty("sino-manager-web") + "/sino-manager-web/manager/category/add_back_category";
    public final static String urlCheckUser=  prop.getProperty("sino-manager-web") +        "/sino-manager-web/manager/member/user/check_user";
    public final static String loginSino = prop.get("sino-manager-web") + "/sino-manager-web/manager/sys/user/login_user";
    //seller
    public final static String addShopUrl =   prop.getProperty("sino-seller-web") +          "/sino-seller-web/seller/shop/add_shop_info";
    public final static String addItemUrl = prop.get("sino-seller-web") + "/sino-seller-web/seller/item/add_item";
    public final static String onShelfItemUrl = prop.getProperty("sino-seller-web") + "/sino-seller-web/seller/item/on_shelf_item";
    public final static String queryItemPageListUrl = prop.getProperty("sino-seller-web") + "/sino-seller-web/seller/item/query_item_page_list";
    public final static String queryInquiryDetailsListUrl = prop.getProperty("sino-seller-web") + "/sino-seller-web/seller/inquiry/query_inquiry_details_list";
    public final static String quoteUrl = prop.getProperty("sino-seller-web") + "/sino-seller-web/seller/inquiry/quote";
    //buyer
    public final static String urlCreateUser= prop.getProperty("sino-buyer-web") +         "/sino-buyer-web/buyer/user/check_register_verify_code";
    public final static String urlGetUser=    prop.getProperty("sino-buyer-web") +            "/sino-buyer-web/buyer/user/get_user_info";
    public final static String addAddress =   prop.getProperty("sino-buyer-web") +           "/sino-buyer-web/buyer/company/add_address";
    public final static String applySample = prop.getProperty("sino-buyer-web") + "/sino-buyer-web/buyer/sample/apply_sample";
    public final static String switchoverIdentityUrl = prop.getProperty("sino-buyer-web") + "/sino-buyer-web/buyer/user/switch_identity";
    public final static String inquiryUrl = prop.getProperty("sino-buyer-web") + "/sino-buyer-web/buyer/inquiry/inquiry";
    public final static String queryInquiryPageUrl = prop.getProperty("sino-buyer-web") + "/sino-buyer-web/buyer/inquiry/query_inquiry_page";
    public final static String loginSeller = prop.get("sino-buyer-web") + "/sino-buyer-web/buyer/user/login";
    public final static String querySamplePage = prop.get("sino-buyer-web") + "/sino-buyer-web/buyer/sample/query_sample_page";
}
