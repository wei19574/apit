package com.xgAutomation.dataPrepare.sinotrans;

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.ConfigUtils;
import com.xgAutomation.framework.utils.JsonReader.JsonFileReaderUtil;
import com.xgAutomation.framework.utils.JsonUtil.JsonParse;
import com.xgAutomation.framework.utils.StringUtils;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by liuqq on 2018/6/13.
 */
public class DataUtil_seller {



    /**
     * 创建商品，随机的名
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> createItem(OkHttpClient client, RequestFactory re,String sellerAccessToken)throws FrameworkException,IOException {
        //创建产品json数据
        String jsonDefaultItem = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultItem.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultItem);
        //生成随机的字符作为商品
        String itemName = StringUtils.getRandomString(12);
        jsonObject.put("itemName",itemName);
        //登录卖家端
        String accessTokenSeller = "";
        Map<String,Object> map = new HashMap<>();
            if (sellerAccessToken.equals("")){
                accessTokenSeller = Login.login_sellerDefault(client,re);
                jsonObject.put("accessToken",accessTokenSeller);
                map.put("accessToken",accessTokenSeller);
            }else {
                jsonObject.put("accessToken",sellerAccessToken);
                map.put("accessToken",sellerAccessToken);
            }
        Response response = re.okHttpPost_Form(client, Url.addItemUrl, jsonObject.toString());
        String itemNo = JsonParse.getValueByJpathString(response.getBody(),"data/itemNo");
        map.put("itemName",itemName);
        map.put("itemNo",itemNo);
        return map;
    }

    /**
     * 获取未上架的产品编号
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> getItemNo(OkHttpClient client, RequestFactory re,String sellerAccessToken)throws FrameworkException,IOException{
        //获取产品列表
        String jsonDefaultItemPage = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultItemPage.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultItemPage);
        Map<String,Object> map = createItem(client,re,sellerAccessToken);
        String json = ConfigUtils.replaceJson(map,jsonObject.toString());
        String dataString = null;

            Response response = re.okHttpPost_Form(client,Url.queryItemPageListUrl,json);
            dataString = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/itemNo");
        Map<String,Object> mapReturn = new HashMap<>();
        mapReturn.put("itemNo",dataString);
        mapReturn.put("accessToken",map.get("accessToken"));
        return mapReturn;
    }

    /**
     * 获取已经上架的产品编号
     * @param client
     * @param re
     * @return
     */
    public static String getItemNoByCheck(OkHttpClient client, RequestFactory re,String sellerAccessToken)throws FrameworkException,IOException{
        Map<String,Object> map = getItemNo(client,re,sellerAccessToken);
        //审核产品
        String jsonDefaultCheckItem = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultCheckItem.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultCheckItem);
        jsonObject.put("itemNo",map.get("itemNo"));
        jsonObject.put("accessToken",map.get("accessToken"));
            re.okHttpPost_Form(client,Url.onShelfItemUrl,jsonObject.toString());
        return map.get("itemNo").toString();
    }


    /**
     * 买家发起询价
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> inquiry(OkHttpClient client, RequestFactory re)throws FrameworkException,IOException{
        //发起询价
        String jsonDefaultInquiry = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultInquiry.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultInquiry);
        //生成随机的字符作为商品
        String itemTitle = jsonObject.getString("itemTitle");
        //登录卖家端
        String accessToken = "";
            //作为买家登录进行询价
            accessToken = Login.login_buyerDefault(client,re);
            jsonObject.put("accessToken",accessToken);
            re.okHttpPost_Form(client,Url.inquiryUrl,jsonObject.toString());
        Map<String,Object> map = new HashMap<>();
        map.put("itemTitle",itemTitle);
        map.put("accessToken",accessToken);
        return map;
    }

    /**
     * 买家端从询盘单列表分页那边查询之前创建的询盘单号
     * @param client
     * @param re
     * @return
     */
    public static String getInquiryOrderNo(OkHttpClient client, RequestFactory re )throws FrameworkException,IOException{
        //查询询盘单列表 分页
        String jsonDefaultInquiryPage = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultInquiryPage.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultInquiryPage);
        Map<String,Object> map = inquiry(client,re);
        String json = ConfigUtils.replaceJson(map,jsonObject.toString());
        String inquiryOrderNo = "";
            Response response = re.okHttpPost_Form(client, Url.queryInquiryPageUrl, json);
            inquiryOrderNo = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/inquiryOrderNo");

        return inquiryOrderNo;
    }

    /**
     * 获取卖家端报价单
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> getTenderOrderNo(OkHttpClient client, RequestFactory re)throws FrameworkException,IOException{
        //进行询价，获取询价单号
        String inquiryNo = getInquiryOrderNo(client,re);
        String jsonDefaultInquiryDetailsList = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultInquiryDetailsList.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultInquiryDetailsList);
        jsonObject.put("inquiryOrderNo",inquiryNo);
        //卖家端登录
        String accessToken = Login.login_sellerDefault(client,re);
        jsonObject.put("accessToken",accessToken);
        String tenderOrderNo = "";
        Map<String,Object> mp = new HashMap<>();
            //卖家端查询询盘详情列表
            Response response = re.okHttpPost_Form(client,Url.queryInquiryDetailsListUrl,jsonObject.toString());
            String inquirySubOrderNo = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/inquiryInfoVo/inquirySubOrderNo");
            String inquiryOrderNo = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/inquiryInfoVo/inquiryOrderNo");
            //卖家端进行报价
            String jsonDefaultQuote = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultQuote.json");
            JSONObject jsonObject1 = JSONObject.fromObject(jsonDefaultQuote);
            jsonObject1.put("inquiryOrderNo",inquiryOrderNo);
            jsonObject1.put("inquirySubOrderNo",inquirySubOrderNo);
            jsonObject1.put("accessToken",accessToken);
            re.okHttpPost_Form(client,Url.quoteUrl,jsonObject1.toString());
            //再次查询，查询出报价单号
            Response response1 = re.okHttpPost_Form(client,Url.queryInquiryDetailsListUrl,jsonObject.toString());
            tenderOrderNo = JsonParse.getValueByJpath(JSONObject.fromObject(response1.getBody()),"data[0]/tenderInfoVo/tenderOrderNo");
            mp.put("tenderOrderNo",tenderOrderNo);
            mp.put("accessToken",accessToken);
        return mp;
    }

    /**
     * 申请样品，返回样品单号
     * @param client
     * @param re
     * @return
     */
    public static Map<String,Object> getSampleOrderNo(OkHttpClient client, RequestFactory re,String sellerAccessToken)throws FrameworkException,IOException{
        //获取报价单
        Map<String,Object> map = getTenderOrderNo(client,re);
        String accessToken = Login.login_buyerDefault(client, re);
        //买家申请样品
        String jsonDefaultApplySample = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultApplySample.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultApplySample);
        jsonObject.put("accessToken",accessToken);
        if(map.get("tenderOrderNo") == null){
            throw new NullPointerException("tenderOrderNo没有获取到");
        }
        jsonObject.put("tenderOrderNo",map.get("tenderOrderNo"));
        Response response = re.okHttpPost_Form(client,Url.applySample,jsonObject.toString());
        String dataString = null;

            //根据卖家端查询样品单列表查询出样品单号
            String jsonDefaultQuerySamplePage = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultQuerySamplePage.json");
            JSONObject jsonObject1 = JSONObject.fromObject(jsonDefaultQuerySamplePage);
            jsonObject1.put("accessToken",sellerAccessToken);

            jsonObject1.put("queryParam",map.get("inquiryOrderNo"));
            response = re.okHttpPost_Form(client,Url.querySamplePage,jsonObject1.toString());
            dataString = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/sampleOrderNo");
        Map<String,Object> mapReturn = new HashMap<>();
        mapReturn.put("sampleOrderNo",dataString);
        return mapReturn;
    }

    public static String getInquirySubOrderNo(OkHttpClient client,RequestFactory re,String inquiryOrderNo,String sellerAccessToken)throws FrameworkException,IOException{
        //查询询盘单详情
        String jsonDefaultInquiryPage = JsonFileReaderUtil.JsonFileReader("/sinotrans/buyerAndseller/jsonDefaultInquiryDetailsList.json");
        JSONObject jsonObject = JSONObject.fromObject(jsonDefaultInquiryPage);
        Map<String,Object> map = new HashMap<>();
        map.put("inquiryOrderNo",inquiryOrderNo);
        map.put("accessToken",sellerAccessToken);

        String json = ConfigUtils.replaceJson(map,jsonObject.toString());
        String inquirySubOrderNo = "";

            Response response = re.okHttpPost_Form(client, Url.queryInquiryDetailsListUrl, json);
            inquirySubOrderNo = JsonParse.getValueByJpath(JSONObject.fromObject(response.getBody()),"data[0]/inquiryInfoVo/inquirySubOrderNo");
        return inquirySubOrderNo;
    }

    public static void choverldentity(OkHttpClient client, RequestFactory re,String accessToken)throws FrameworkException,IOException{
        if (accessToken == null){
            throw new FrameworkException("登录失败");
        }
        Map<String,String> map = new HashMap<>();
        map.put("accessToken",accessToken);
        JSONObject jsonObject = JSONObject.fromObject(map);
        re.okHttpPost_Form_paramAccessToken(client,Url.switchoverIdentityUrl,jsonObject.toString());
    }

    public static void main(String[] args) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        RequestFactory re = new RequestFactory();
        Map<String,String> mp = new HashMap<>();
        mp.put("email","13023696650@163.com ");
        mp.put("password","123456");

        try {
            String ino = getInquiryOrderNo(client,re);
            String sellerToken = Login.login_seller(client,re,mp);
            System.out.println(getInquirySubOrderNo(client,re,ino,sellerToken));
        } catch (FrameworkException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
