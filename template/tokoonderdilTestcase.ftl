package ${packageDir};
import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.ProjectHost;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.CsvReader.csvUtils;
import com.xgAutomation.framework.meta.LogType;
import okhttp3.OkHttpClient;
import com.xgAutomation.framework.utils.LogReader.LogService;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;
import java.util.HashMap;
import java.util.Map;
import com.xgAutomation.framework.utils.Interceptor.HeaderInterceptor;
import com.xgAutomation.framework.request.TokenHeaderInterceptor;
import net.sf.json.JSONObject;
import java.util.concurrent.TimeUnit;

public class ${interface_name} {
    RequestFactory re;
    OkHttpClient client;

    @BeforeTest
    public void setUp() {
        re = new RequestFactory();
        client = new OkHttpClient.Builder()
                .addInterceptor(new TokenHeaderInterceptor())
                .addInterceptor(new HeaderInterceptor())
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20,TimeUnit.SECONDS)
                .build();
    }

    @Test(dataProvider = "dataPro", testName = "${interface_name}Test")
    public void ${interface_name}Test(String project,String testcaseDescription,${input_param}String ExpectedCode, String ExpectedMessage,String ExpectedData,String TestCaseType,String isRun) {
        if (isRun.toUpperCase().equals("Y")) {
            LogService.info(${interface_name}.class,LogType.BIZ,"开始执行——"+testcaseDescription);
            Map<String, Object> bizparams = new HashMap<String, Object>();
${sourceParam}
            Response response;
            try {
                response = re.okHttpPost_Json(client, "${interfaceUrl}", bizparams, ProjectHost.Tokoonderdil_Supplier);
                if(response == null){
                    throw new NullPointerException("获取的response为空");
                }
                JSONObject json = JSONObject.fromObject(response.getBody());
                String data = json.getString("data");
                AssertUtil.checkCode(response.getCode(), ExpectedCode);
                AssertUtil.checkMessage(json, ExpectedMessage,"message");
                AssertUtil.checkData(data, ExpectedData);
                LogService.info(${interface_name}.class,LogType.BIZ,"执行结束——"+testcaseDescription);
            } catch (FrameworkException e) {
                e.printStackTrace();
            }
        }else{
            assertTrue(false, "isRUN为N，未运行");
        }
    }

    @DataProvider
    public Object[][] dataPro() {
        return csvUtils.csvReader("${csvDir}/${interface_name}.csv");
    }


}
