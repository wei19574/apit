package ${packageDir};

import com.xgAutomation.framework.exception.FrameworkException;
import com.xgAutomation.framework.meta.LogType;
import com.xgAutomation.framework.meta.Response;
import com.xgAutomation.framework.request.RequestFactory;
import com.xgAutomation.framework.utils.Assert.AssertUtil;
import com.xgAutomation.framework.utils.LogReader.LogService;
import com.xgAutomation.framework.utils.StringUtil;
import okhttp3.OkHttpClient;


public class ${interfaceName} {

    public Boolean ${interfaceMethod}Test(String url,String json,String expectedBody){
        RequestFactory re = new RequestFactory();
        OkHttpClient client = new OkHttpClient.Builder().build();
        Boolean result = true;
        Response response;
        String jsonBody;
        try {
            jsonBody = StringUtil.checkJson(client,re,json);
            response = re.okHttp_Post_Testlink(client,url,jsonBody);
            AssertUtil.checkTestlinkResult(response.getBody(),expectedBody);
        }catch (FrameworkException e) {
            LogService.error(${interfaceName}.class,LogType.BIZ,e.getMessage());
            result = false;
        }catch (Exception f) {
            LogService.error(${interfaceName}.class,LogType.BIZ,f.getMessage());
            result = false;
        }catch (Error error){
            LogService.error(${interfaceName}.class,LogType.Assert,error.getMessage());
            result = false;

        }finally {
            return result;
        }
    }

}
